Making Cloud Photo Enhancement Worthwhile with Compact Transform Recipes
========================================================================

Michael Gharbi          <gharbi@mit.edu>,
Yichang Shih            <yichang@mit.edu>,
Sylvain Paris           <sparis@adobe.com>,
Jonathan Ragan-Kelley   <jrk@cs.stanford.edu>,
Fredo Durand            <fredo@mit.edu>


Installation
------------

Run the convenience installation script:

    ./install.py

Alternatively, follow the steps below.
To install the required python packages and the versions used for development:

    pip install -r requirements.txt

In particular you will need to use Django 1.6 (version 1.7 conflicts with South migration).

After installing the required python packages, create the sqlite database by
running the following commands:

    python analysis/manage.py syncdb
    python analysis/manage.py schemamigration summary --init
    python analysis/manage.py migrate

Compile the cpp-extensions:

    cd transform_compression/mghimproc && python setup.py build

You should be all set!


Usage
-----

You can run the example using the following command:

    python job/evaluate.py example

The entry-point script to the algorithm is `transform_compression/run.py`. We
recommend using the utility script `job/evaluate.py` to run the program. A typical usage is to
run the algorithm on `n` images from category `cat`:

    python job/evaluate.py cat --limit n

This script allows local execution as well as a remote cluster execution using
the condor job management system.


Folder structure
----------------

`analysis/` contains code related to the web interface, database models and the display views.

`config/` contains the various algorithm parameters for different scenarios.

`data/` contains the input data.

`job/` contains the entry point to the algorithm for both local and remote cluster execution.

`output/` is where all the generated data as well as the sqlite database are stored.

`scripts/` is a collection of utility scripts for data collection, data
renaming, figures and tables plotting, and supplemental web material.

`test/` contains a collection of unit test scripts

`transform_compression/` holds the core of our algorithm.

The main algorithms for recipe fitting and reconstruction can be found in
`transform_compression/processor/transform_model.py`.


Data format
-----------

It is assumed in the code that the input data is stored in the `data/` folder .
For an image filter (also referred as category) *filt*, an input/output pair
*img*, there should be the following images:

    `data/filt/img/unprocessed.ext`
    `data/filt/img/processed.ext`

To hold the unprocessed and processed images. *ext* is any image format
extension, we currently handle png and jpeg.
