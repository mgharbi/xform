"""
Django settings for analysis project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#f=_wl^u5$-zyra00bn2f5c#2^kgme5!(g@s@c+^2@%oj7p=7^'

# SECURITY WARNING: don't run with debug turned on in production!
if os.getenv("DJANGO_ENV") == "PROD":
    DEBUG = True
    TEMPLATE_DEBUG = True
else:
    DEBUG = True
    TEMPLATE_DEBUG = True


ALLOWED_HOSTS = []

# Application definition


GIT_REPO = 'https://bitbucket.org/mgharbi/transformcompression/commits/'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django_extensions',
    'annoying',
    'summary',
    # 'south',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'analysis.urls'

WSGI_APPLICATION = 'analysis.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

if os.getenv("DJANGO_ENV") == "PROD":
    # DATABASES = {
    #          'default': {
    #              'ENGINE': 'django.db.backends.mysql',
    #              'NAME': 'xform_transformation',
    #              'USER': 'xform',
    #              'PASSWORD':'xformpass',
    #              'HOST':'mysql.csail.mit.edu',
    #              'PORT': '3306',
    #          }
    # }
    DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': os.path.join(BASE_DIR,'..','output_prod','prod.sqlite3'),
            }
    }
else:
    DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': os.path.join(BASE_DIR,'..','output','dev.sqlite3'),
            }
    }


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

if os.getenv("DJANGO_ENV") == "PROD":
    # STATIC_URL = '/gharbi/xform_compression/static/'
    # STATIC_ROOT = '/afs/csail.mit.edu/u/g/gharbi/public_html/xform_compression/static/'
    # MEDIA_ROOT = '/afs/csail.mit.edu/u/g/gharbi/public_html/xform_compression/media/'
    # MEDIA_URL = '/gharbi/xform_compression/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR,'..')
    MEDIA_URL = '/media/'
    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR,'static')
else:
    MEDIA_ROOT = os.path.join(BASE_DIR,'..')
    MEDIA_URL = '/media/'
    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR,'static')


