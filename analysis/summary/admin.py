from django.contrib import admin
from summary.models import Experiment, Category, Result, Parameters

admin.site.register(Experiment)
admin.site.register(Category)
admin.site.register(Result)
admin.site.register(Parameters)


