# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Experiment',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('date', models.DateField(auto_now_add=True)),
                ('git_commit', models.CharField(default=b'', max_length=50, blank=True)),
                ('description', models.TextField(default=b'', blank=True)),
                ('categories', models.ManyToManyField(to='summary.Category', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Parameters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transform_model', models.CharField(max_length=200)),
                ('pipeline', models.CharField(default=b'', max_length=500)),
                ('in_downsampling', models.IntegerField(default=1)),
                ('in_quality', models.IntegerField(default=100)),
                ('add_noise', models.BooleanField(default=False)),
                ('upsample', models.BooleanField(default=False)),
                ('epsilon', models.FloatField(default=0)),
                ('wSize', models.IntegerField(default=0)),
                ('use_patch_overlap', models.BooleanField(default=False)),
                ('use_ratio_of_residuals', models.BooleanField(default=False)),
                ('use_multiscale_feat', models.BooleanField(default=False)),
                ('use_tonecurve_feat', models.BooleanField(default=False)),
                ('use_stack', models.BooleanField(default=False)),
                ('luma_bands', models.IntegerField(default=6)),
                ('ms_levels', models.IntegerField(default=-1)),
                ('recipe_format', models.CharField(default=b'.jpg', max_length=100)),
                ('transfer_multiscale', models.BooleanField(default=False)),
                ('experiment', models.OneToOneField(to='summary.Experiment')),
            ],
            options={
                'verbose_name_plural': 'parameters',
            },
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('outputPath', models.CharField(default=b'', max_length=400, blank=True)),
                ('dataPath', models.CharField(default=b'', max_length=400, blank=True)),
                ('psnr', models.FloatField(default=0)),
                ('ssim', models.FloatField(default=0)),
                ('computation_time', models.FloatField(default=0)),
                ('compression_up', models.FloatField(default=0)),
                ('compression_down', models.FloatField(default=0)),
                ('error', models.CharField(default=b'', max_length=400, blank=True)),
                ('max_error_x', models.IntegerField(default=-1)),
                ('max_error_y', models.IntegerField(default=-1)),
                ('category', models.ForeignKey(to='summary.Category')),
                ('experiment', models.ForeignKey(to='summary.Experiment')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='result',
            unique_together=set([('category', 'experiment', 'name')]),
        ),
    ]
