def str2bool(s):
    return s.lower() in ["True", "true", "1", "y", "yes"]
