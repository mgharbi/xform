$(document).ready(function() {
    var img = $("img#result_processed");
    img.on("mouseover",function() {
        img.attr("src", processedPath);
        $("#alternate_title").text("reference")
    });
    img.on("mouseout",function() {
        img.attr("src", reconstructedPath);
        $("#alternate_title").text("reconstructed")
    });
});
