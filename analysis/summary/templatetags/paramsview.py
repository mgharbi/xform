from django import template
register = template.Library()
@register.filter()
def p_winsize(val):
    return "%dpx" % (val)
@register.filter()
def p_compression(val):
    if type(val) is float:
        return "%.1f%%" % (100*val)
@register.filter()
def p_maxsize(val):
    return "%dpx" % val
@register.filter()
def p_psnr(val):
    if type(val) is float:
        return "%.1f" %val
@register.filter()
def p_ssim(val):
    if type(val) is float:
        return "%.2f" %val
@register.filter()
def p_time(val):
    if val<1:
        return "%.2fs" %val
    else:
        return "%.0fs" %val
