from django import template
from analysis.settings import MEDIA_URL, BASE_DIR
import os

def fuse_path(dname, fname):
    d = os.path.split(BASE_DIR)[0]
    d = dname.replace(d,"")
    d = os.path.join(d, fname)
    return MEDIA_URL+d

register = template.Library()
@register.filter()
def unprocessed_path(val):
    return fuse_path(val,"unprocessed.png")

@register.filter()
def processed_path(val):
    return fuse_path(val,"processed.png")

@register.filter()
def reconstructed_path(val):
    return fuse_path(val,"reconstructed.png")
