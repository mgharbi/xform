from django.conf.urls import patterns, url

from summary import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^delete_all$', views.delete_all, name='delete_all'),
    url(r'^delete/([0-9]*)$', views.delete, name='delete'),
    url(r'^experiment/([0-9]*)$', views.experiment_summary, name='experiment_summary'),
    url(r'^experiment/([0-9]*)/category/([0-9]*)$', views.results_per_category, name='category_summary'),
    url(r'^result/([0-9]*)$', views.result, name='result'),
)
