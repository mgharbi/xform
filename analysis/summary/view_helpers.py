from summary.models import Category
from summary.models import Result
from summary.models import Experiment

class CategorySummary:
    psnr = 0
    ssim = 0
    count = 0
    compression_down = 0
    compression_up = 0
    minPSNR = float("inf")
    minPSNR_id = 0
    minSSIM = float("inf")
    minSSIM_id = 0
    maxLAB = 0
    maxLAB_id = 0
    errors = 0
    def summary(self,c,expId):
        res = Result.objects.filter(category__name=c.name,experiment__id=expId)
        self.category_id = c.id
        self.name = c.name
        for r in res:
            self.count += 1
            if r.error:
                # Do not count errored results
                self.errors += 1
                continue
            self.psnr += r.psnr
            self.ssim += r.ssim
            self.compression_down += r.compression_down
            self.compression_up += r.compression_up
            if r.psnr < self.minPSNR:
                self.minPSNR = r.psnr
                self.minPSNR_id = r.id
            if r.ssim < self.minSSIM:
                self.minSSIM = r.ssim
                self.minSSIM_id = r.id
        nValid = self.count - self.errors
        if nValid > 0:
            self.psnr /= nValid
            self.ssim /= nValid
            self.compression_down /= nValid
            self.compression_up /= nValid

    def aggregate(self,name,s_list):
        self.name = name
        nclass = 0
        for s in s_list:
            nValid = s.count - s.errors
            self.errors += s.errors
            if nValid >0:
                nclass += 1
            self.psnr += s.psnr
            self.count+= s.count
            self.ssim += s.ssim
            self.compression_down += s.compression_down
            self.compression_up += s.compression_up
            if s.psnr < self.minPSNR:
                self.minPSNR = s.minPSNR
                self.minPSNR_id = s.minPSNR_id
            if s.ssim < self.minSSIM:
                self.minSSIM = s.minSSIM
                self.minSSIM_id = s.minSSIM_id
        nValid = self.count - self.errors
        if nclass > 0:
            self.psnr /= nclass
            self.ssim /= nclass
            self.compression_down /= nclass
            self.compression_up /= nclass


def getSummary(category_list, expId):
    category_data = []
    all_cat_summary = CategorySummary()
    for c in category_list:
        s = CategorySummary()
        s.summary(c, expId)
        if s.count > 0:
            category_data.append(s)
    all_cat_summary.aggregate("All", category_data)
    if all_cat_summary.count > 0:
        category_data.insert(0,all_cat_summary)
    return category_data
