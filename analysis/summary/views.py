import os

from django.shortcuts import render, redirect
from django.conf import settings
from django.template import RequestContext

from summary.models import Category
from summary.models import Result
from summary.models import Experiment

from view_helpers import *

def index(request):
    e = Experiment.objects.all().order_by("id")

    for exp in e:
        category_list = Category.objects.filter(experiment=exp).order_by("name")
        category_data = getSummary(category_list, exp.id)
        if len(category_data)>0:
            summary = category_data[0]
            exp.summary = summary

    isdebug = True
    if os.getenv("DJANGO_ENV") == "PROD":
        isdebug = False

    context = RequestContext(request, {
        'experiments': e,
        'debug': isdebug,
    })
    return render(request,'summary/index.html', context)

def experiment_summary(request, expId):
    e = Experiment.objects.get(id=expId)
    category_list = Category.objects.filter(experiment=e).order_by("name")
    category_data = getSummary(category_list, expId)

    context = RequestContext(request, {
        'category_data': category_data,
        'experiment': e,
        'git_repo': settings.GIT_REPO,
    })
    return render(request,'summary/experiment_summary.html', context)

def result(request, resultId):
    r = Result.objects.get(id=resultId)
    context = RequestContext(request, {
        'result': r,
    })
    return render(request,'summary/result.html', context)

def results_per_category(request,expId, catId):
    e = Experiment.objects.get(id=expId)
    c = Category.objects.get(id=catId)
    results =  Result.objects.filter(category=catId, experiment=e).order_by("name")
    context = RequestContext(request, {
        'category': c,
        'experiment':e,
        'results': results,
    })
    return render(request,'summary/category_summary.html', context)

def delete_all(request):
    Experiment.objects.all().delete()
    return redirect('index')
def delete(request, expId):
    Experiment.objects.get(id=expId).delete()
    return redirect('index')
