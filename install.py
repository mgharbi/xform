import os
import subprocess

# Install dependencies
os.system("pip install -r requirements.txt")

# Install db
os.system("python analysis/manage.py reset_db")
os.system("python analysis/manage.py syncdb")
os.system("python analysis/manage.py schemamigration summary --init")
os.system("python analysis/manage.py migrate")

# Compile C code
os.system("cd transform_compression/mghimproc && python setup.py build")

