#!/usr/bin/python
# -------------------------------------------------------------------
# File:    evaluate.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2014-11-05
# -------------------------------------------------------------------
#
# Evaluation entry point.
#
# Enables interface for both local evaluation and on the condor cluster.
#
# ------------------------------------------------------------------#

import sys, os
import subprocess

# Virtual env
# if os.getenv("DJANGO_ENV") == "PROD":
#     venv = '/afs/csail.mit.edu/u/g/gharbi/.virtualenvs/default/bin/activate_this.py'
#     execfile(venv, dict(__file__=venv))
#     sys.path.insert(0, "/afs/csail.mit.edu/u/g/gharbi/.virtualenvs/default")

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "transform_compression"))

#-----------------------------------------------------------------------------------------------------------

import argparse
from settings import *
import utils.filesystem as fs
from processor.processor import Processor

allCat = fs.listSubdir(DATA_DIR)
cfg = os.listdir(CONFIG_DIR)
processing_modes = [f.replace(".cfg","") for f in cfg]

# Django database connection
sys.path = sys.path + [os.path.join(BASE_DIR,"analysis")]
if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE","analysis.settings")
from summary.models import Result, Experiment, Category, Parameters
import django
django.setup()

def override_params(e, args):
    p = e.parameters

    if args.wSize:
        p.wSize = args.wSize
    if args.input_downsampling:
        p.in_downsampling = args.input_downsampling
    if args.input_quality:
        p.in_quality = args.input_quality
    if args.luma_bands:
        p.luma_bands = args.luma_bands
    if args.epsilon:
        p.epsilon = args.epsilon

    if args.patch_overlap is not None:
        p.use_patch_overlap = args.patch_overlap
    if args.ratio_residual is not None:
        p.use_ratio_of_residuals = args.ratio_residual
    if args.multiscale_features is not None:
        p.use_multiscale_feat = args.multiscale_features
    if args.tonecurve_features is not None:
        p.use_tonecurve_feat = args.tonecurve_features
    if args.noise is not None:
        p.add_noise = args.noise
    if args.upsample is not None:
        p.upsample = args.upsample
    if args.transfer is not None:
        p.transfer_multiscale = args.transfer

    p.save()

def main(args):
    # Prepare experiment
    label = ""
    if args.update:
        if Experiment.objects.filter(id=args.update).exists():
            e = Experiment.objects.get(id=args.update)
            label = "(UPDATE)"
        else:
            raise NameError("No such experiement exists: %d" % args.update)
    else:
        e = Experiment()
        e.save()

    # Load params, unless we're updating
    if not args.update:
        # Get current git commit
        try:
            git_commit = subprocess.check_output(['git','rev-parse','HEAD']).rstrip('\n')
        except:
            git_commit = ''
        e.git_commit = git_commit
        if args.m:
            e.description = args.m
        else:
            e.description = "debug experiment"
        config_file = os.path.join(CONFIG_DIR,args.mode+".cfg")
        e.parameters.load(DEFAULT_CONFIG_FILE)
        e.parameters.load(config_file)
        override_params(e, args)
        e.save()

    print "\nExperiment %s %s" % (e.__str__(),label)

    # List of categories to process
    if args.categories == ["all"]:
        categories = allCat
    else:
        categories = args.categories
    if args.f and len(categories)>1:
        raise ValueError("Specify a category as well as the example name")

    # Condor dump files on the cluster
    # if os.getenv("DJANGO_ENV") == "PROD":
    #     scratchDir = os.path.join("/data","scratch","gharbi","transformcompression",str(e.id))
    #     if not os.path.exists(scratchDir):
    #         os.makedirs(scratchDir)

    for category in categories:
        inputDir = os.path.join(DATA_DIR, category)
        if not os.path.exists(inputDir):
            raise NameError("No such category exists: %s",category)

        if args.f:
            files = [args.f]
        else:
            files = fs.listSubdir(inputDir)

        if (args.limit > 0) and (len(files)>args.limit):
            files = files[0:args.limit]
        if Category.objects.filter(name=category).exists():
            c = Category.objects.get(name=category)
        else:
            c      = Category()
            c.name = category
            c.save()
        e.categories.add(c)
        e.save()

        print "  * Dataset %s (%d)" % (category,len(files))

        for ff in files:
            exists = Result.objects.filter(category=c, experiment=e, name=ff).exists()
            if exists:
                r = Result.objects.get(category=c, experiment=e, name=ff)
                if args.update and not r.error:
                    print "    - %s: skipped" % ff
                    continue
            print "    - %s" % ff
            # if os.getenv("DJANGO_ENV") == "PROD":
            #     job_name = "%s-%s-%s" % (e.id, c, ff)
            #
            #     f = open(job_name,'w')
            #     header = [
            #             "# Condor Header\n"
            #             "GetEnv = True\n",
            #             "Universe = vanilla\n",
            #             "should_transfer_files = IF_NEEDED\n",
            #             "WhenToTransferOutput = ON_EXIT\n",
            #             'requirements = (OpSys == "LINUX")\n',
            #             "\n",
            #             ]
            #     f.writelines(header)
            #
            #     body = [
            #             "# Job\n",
            #             "Executable ="+os.path.join(BASE_DIR, "transform_compression", "run.py")+"\n",
            #             "Arguments = %d %s %s\n" %(e.id, c, ff),
            #             "output = "+os.path.join(scratchDir, "%s-%s.out" % (category,ff))+"\n",
            #             "error = "+os.path.join(scratchDir,"%s-%s.err" % (category,ff))+"\n",
            #             "log = "+os.path.join(scratchDir,"%s-%s.log" % (category,ff))+"\n",
            #             "queue\n"
            #             ]
            #     f.writelines(body)
            #     f.close()
            #
            #     os.system("condor_submit %s" % job_name)
            #     os.remove(job_name)
            # else:
            os.system("python %s/transform_compression/run.py %d %s %s" % (BASE_DIR, e.id, category, ff))

if __name__ == '__main__':
    # Input parsing
    parser = argparse.ArgumentParser("Evaluate the algorithm on given categories.")
    parser.add_argument('categories', nargs='*',default='all',choices=allCat+['all'], help='list of categories to process')
    parser.add_argument('--update', type=int, help="add or update experiment")
    parser.add_argument('-f', default='',help='specific image to evaluate')
    parser.add_argument('--limit', type=int, default=-1,help='maximum number of images to process per category')
    parser.add_argument('-m', default="",help="description of the experiment")

    parser.add_argument('--mode', default='default', choices = processing_modes, help='algorithm to run')

    parser.add_argument('--wSize',              type=int, help='size of the window')
    parser.add_argument('--input-downsampling', type=int, help='downsize of input')
    parser.add_argument('--input-quality',      type=int, help='quality of input')
    parser.add_argument('--luma-bands',         type=int, help='number of bins for the luma curve')
    parser.add_argument('--ms-levels',          type=int, help='number of pyramid levels')
    parser.add_argument('--epsilon',            type=float, help='regularization')

    parser.add_argument('--patch-overlap',          dest = 'patch_overlap', action = 'store_true')
    parser.add_argument('--no-patch-overlap',       dest = 'patch_overlap', action = 'store_false')
    parser.add_argument('--ratio-residual',         dest = 'ratio_residual', action = 'store_true')
    parser.add_argument('--no-ratio-residual',      dest = 'ratio_residual', action = 'store_false')
    parser.add_argument('--multiscale-features',    dest = 'multiscale_features', action = 'store_true')
    parser.add_argument('--no-multiscale-features', dest = 'multiscale_features', action = 'store_false')
    parser.add_argument('--tonecurve-features',     dest = 'tonecurve_features', action = 'store_true')
    parser.add_argument('--no-tonecurve-features',  dest = 'tonecurve_features', action = 'store_false')
    parser.add_argument('--noise',     dest = 'noise', action = 'store_true')
    parser.add_argument('--no-noise',  dest = 'noise', action = 'store_false')
    parser.add_argument('--upsample',     dest = 'upsample', action = 'store_true')
    parser.add_argument('--no-upsample',  dest = 'upsample', action = 'store_false')
    parser.add_argument('--transfer',     dest = 'transfer', action = 'store_true')
    parser.add_argument('--no-transfer',  dest = 'transfer', action = 'store_false')

    parser.set_defaults(patch_overlap=None)
    parser.set_defaults(ratio_residual=None)
    parser.set_defaults(multiscale_features=None)
    parser.set_defaults(tonecurve_features=None)
    parser.set_defaults(noise=None)
    parser.set_defaults(upsample=None)
    parser.set_defaults(transfer=None)

    args = parser.parse_args()

    main(args)
