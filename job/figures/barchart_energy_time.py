import os,sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

#-----------------------------------------------------------------------------------------------------------

from shared import jpeg_color, recipe_color, local_color, color_blind
import numpy as np
import scipy.interpolate as interpolate
import matplotlib.pyplot as plt

import pandas
import locale

import csv
output_dir = "output"
out_time = os.path.join(output_dir,"filter_time.pdf")
out_energy = os.path.join(output_dir,"filter_energy.pdf")

# Use tex
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('legend', fontsize=7)
plt.rc('font', size=7)

data = pandas.read_csv('energy_time.csv',
                       dtype = { '':np.float32, 'filter': str}
)



##########
#  Time  #
##########

n_filters = 5
width = 0.3
spacing = 0.3
pos = np.arange(n_filters)*(3*width+spacing)

fig = plt.figure(figsize=(3.33,2),dpi=300)
ax = fig.add_subplot(1,1,1)

rects1 = ax.bar(pos, data.local_time, width, color = local_color, yerr = data.local_time_var, linewidth = 0, ecolor = color_blind[3], error_kw={'linewidth':0.5})
rects2 = ax.bar(pos+width, data.jpeg_time, width, color = jpeg_color, yerr = data.jpeg_time_var, linewidth = 0, ecolor = color_blind[3], error_kw={'linewidth':0.5})
rects3 = ax.bar(pos+2*width, data.recipe_time, width, color = recipe_color, yerr = data.recipe_time_var, linewidth = 0, ecolor = color_blind[3], error_kw={'linewidth':0.5})

leg = [
    r"local processing",
    r"jpeg cloud",
    r"our recipes",
]
ax.legend((rects1,rects2,rects3), leg, loc = 'upper right', bbox_to_anchor=(.48,1.05), ncol =1, frameon = False)

ax.set_xticks(pos+width*1.5)
xlabels = [
    "Local\nLaplacian",
    "Colorization",
    "Style\nTransfer",
    "Portrait\nTransfer",
    "Time\nof Day"
]
ax.set_xticklabels(xlabels, rotation=00, ha = 'center')

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

ax.set_ylabel(r"Processing time (in s, lower is better)", rotation = 0, position = (0.5,1.1), ha='left', va='center')

plt.savefig(out_time, transparent = True, bbox_inches='tight', pad_inches=0.05)
plt.clf()

##########
#  Energy  #
##########

pos = np.arange(n_filters)*(3*width+spacing)

fig = plt.figure(figsize=(3.33,2),dpi=300)
ax = fig.add_subplot(1,1,1)

rects1 = ax.bar(pos, data.local_energy, width, color = local_color, yerr = data.local_energy_var, linewidth = 0, ecolor = color_blind[3], error_kw={'linewidth':0.5})
rects2 = ax.bar(pos+width, data.jpeg_energy, width, color = jpeg_color, yerr = data.jpeg_energy_var, linewidth = 0, ecolor = color_blind[3], error_kw={'linewidth':0.5})
rects3 = ax.bar(pos+2*width, data.recipe_energy, width, color = recipe_color, yerr = data.recipe_energy_var, linewidth = 0, ecolor = color_blind[3], error_kw={'linewidth':0.5})

ax.legend((rects1,rects2,rects3), leg, loc = 'upper right', bbox_to_anchor=(.48,1.05), ncol =1, frameon = False)

ax.set_xticks(pos+width*1.5)
xlabels = [
    "Local\nLaplacian",
    "Colorization",
    "Style\nTransfer",
    "Portrait\nTransfer",
    "Time\nof Day"
]
ax.set_xticklabels(xlabels, rotation=00, ha = 'center')

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

ax.set_ylabel(r"Energy consumption (in J, lower is better)", rotation = 0, position = (0.5,1.1), ha='left', va='center')

plt.savefig(out_energy, transparent = True, bbox_inches='tight', pad_inches=0.05)
plt.clf()
