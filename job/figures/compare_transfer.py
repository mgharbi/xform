import os,sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

#-----------------------------------------------------------------------------------------------------------

from shared import Settings, SettingsJPEG, color_blind,get_stats_per_category
import numpy as np
import scipy.interpolate as interpolate
import matplotlib.pyplot as plt
# Use tex
# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')
plt.rc('legend', fontsize=9)
plt.rc('font', size=9)

def old_vs_new_psnr(outpath):
    catnames, psnr = get_stats_per_category(ids = [26, 80])[0:2]

    mini = 30
    maxi = 50
    fig = plt.figure(figsize=(4,4))
    ax = fig.add_subplot(1,1,1)
    ax.plot([mini,maxi],[mini,maxi], label = "")
    for j in range(psnr.shape[1]):
        ax.scatter(psnr[0,j], psnr[1,j], c = color_blind[j], linewidth=0, s= 100)
    ax.set_ylim(mini,maxi)
    ax.set_xlim(mini,maxi)
    catnames.insert(0,"")
    ax.set_title("wSize=128 8x8 downsampling")
    ax.set_xlabel("old average psnr (with 'cheat' for style transfer)")
    ax.set_ylabel("new average psnr")
    plt.axis('equal')
    ax.legend(catnames, loc = 'upper left', bbox_to_anchor=(1, 1), ncol =1, frameon = False)
    plt.savefig(outpath, transparent = True, bbox_inches='tight', pad_inches=0.05)
    plt.clf()

if __name__ == '__main__':
    output_dir = "output"
    old_vs_new_psnr(os.path.join(output_dir,"old_vs_new_psnr.pdf"))
