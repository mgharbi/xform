import os,sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

#-----------------------------------------------------------------------------------------------------------

from shared import jpeg_color, recipe_color, local_color, color_blind
import numpy as np
import scipy.interpolate as interpolate
import matplotlib.pyplot as plt

import pandas
import locale

import csv
output_dir = "output"
out_time = os.path.join(output_dir,"time_vs_bw.pdf")
out_energy = os.path.join(output_dir,"energy_vs_bw.pdf")

# Use tex
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('legend', fontsize=7)
plt.rc('font', size=8)

data = pandas.read_csv('bw_vs_style_transfer.csv',
    dtype = np.float32,
)

bw = data.bandwidth
log_bw = np.log(bw)

local_time    = data.local_time
naive_time    = data.naive_time
recipe_time   = data.recipe_time
local_energy  = data.local_energy
naive_energy  = data.naive_energy
recipe_energy = data.recipe_energy

##########
#  Time  #
##########
figwidth = 3.33
# figwidth = 1.6

fig = plt.figure(figsize=(figwidth,2),dpi=300)
ax = fig.add_subplot(1,2,2)

jpeg = ax.plot(log_bw,naive_time, marker="",markersize=10, linewidth = 2, c = jpeg_color)
recipe = ax.plot(log_bw,recipe_time, marker="",markersize=10, linewidth = 2, c = recipe_color)
local = ax.plot(log_bw,local_time, marker="",markersize=10, linewidth = 1, c = local_color)

ax.set_ylim(0,60)
ax.set_xlim(np.log(100),np.log(32000))

ax.tick_params(labeltop='off') # don't put tick labels at the top
ax.xaxis.tick_bottom()

xlabels = []
for val in bw:
    if val >=1000:
        xlabels.append("%d" % (val/1000.0))
    else:
        xlabels.append("%.1f" % (val/1000.0))

ax.set_xticks(log_bw[0::2])
ax.set_xticklabels(xlabels[0::2])

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')

ax.set_xlabel(r'Bandwidth (Mbps)')
# ax.yaxis.set_label_position('right')
ax.set_ylabel("Processing time\n(in s, lower is better)", rotation = 0, position = (0.5,1.15), ha='left', va='center')


leg = [
    r"local processing",
    r"jpeg cloud",
    r"our recipes",
]
ax.legend((local[0], jpeg[0],recipe[0]),leg, loc = 'lower center', bbox_to_anchor=(-.1,-.4), ncol =3, frameon = False)

# ax.legend((local[0], jpeg[0],recipe[0]),leg, loc = 'lower center', bbox_to_anchor=(.5,-.6), ncol =1, frameon = False)

# plt.savefig(out_time, transparent = True, bbox_inches='tight', pad_inches=0.05)
# plt.clf()

##########
#  Energy  #
##########

# fig = plt.figure(figsize=(figwidth,2),dpi=300)
ax = fig.add_subplot(1,2,1)

jpeg = ax.plot(log_bw,naive_energy, marker="",markersize=10, linewidth = 2, c = jpeg_color)
recipe = ax.plot(log_bw,recipe_energy, marker="",markersize=10, linewidth = 2, c = recipe_color)
local = ax.plot(log_bw,local_energy, marker="",markersize=10, linewidth = 1, c = local_color)

ax.set_ylim(0,200)
ax.set_xlim(np.log(100),np.log(32000))

ax.tick_params(labeltop='off') # don't put tick labels at the top
ax.xaxis.tick_bottom()

ax.set_xticks(log_bw[0::2])
ax.set_xticklabels(xlabels[0::2])

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')

ax.set_xlabel(r'Bandwidth (Mbps)')
ax.set_ylabel("Energy consumption\n(in J, lower is better)", rotation = 0, position = (0.5,1.15), ha='left', va='center')

fig.subplots_adjust(hspace=.5)

plt.savefig(out_energy, transparent = True, bbox_inches='tight', pad_inches=0.05)
plt.clf()
