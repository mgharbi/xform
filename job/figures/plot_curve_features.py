import os,sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

#-----------------------------------------------------------------------------------------------------------

from shared import Settings, SettingsJPEG, color_blind,get_stats_per_category
import numpy as np
import scipy.interpolate as interpolate
import matplotlib.pyplot as plt
# Use tex
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('legend', fontsize=7)
plt.rc('font', size=8)

CATEGORIES = [
    "Local Laplacian",
    "Dehazing",
    "Detail Manipulation",
    "$L_0$",
    "Matting",
    "Photoshop",
    "Recoloring",
    "Portrait Transfer",
    "Time of Day",
    "Style Transfer",
]

def psnr_vs_wsize(outpath):
    settings = [
        Settings(16  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
        Settings(32  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
        Settings(64  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
        Settings(128 , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
        Settings(256 , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
    ]

    messages = [s.make_mess() for s in settings]

    catnames, psnr = get_stats_per_category(messages)[0:2]
    catnames = CATEGORIES
    x_axis = [16,32,64,128,256]
    fig = plt.figure(figsize=(3.5,2.5),dpi=300)
    ax = fig.add_subplot(1,1,1)
    for j in range(psnr.shape[1]):
        ax.plot(x_axis, psnr[:,j], marker=".",markersize=10, linewidth = 2, c = color_blind[j])
    ax.set_xlabel(r'block size $w$')
    ax.set_ylabel(r"PSNR (dB)")
    ax.set_ylim(25,55)
    ax.set_xlim(16,156)
    ax.set_xticks([16,32,64,128,256])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.legend(catnames, loc = 'upper right', bbox_to_anchor=(1,1), ncol =2, frameon = False)

    plt.savefig(outpath, transparent = True, bbox_inches='tight', pad_inches=0.05)
    plt.clf()


def psnr_vs_nluma(outpath):
    settings = [
        Settings(32  , 1 , 100 , 6  , True  , True  , False , True  , True ) ,
        Settings(32  , 1 , 100 , 2  , True  , True  , True  , True  , True)  ,
        Settings(32  , 1 , 100 , 4  , True  , True  , True  , True  , True)  ,
        Settings(32  , 1 , 100 , 6  , True  , True  , True  , True  , True)  ,
        Settings(32  , 1 , 100 , 8  , True  , True  , True  , True  , True)  ,
        Settings(32  , 1 , 100 , 10 , True  , True  , True  , True  , True)  ,
    ]

    messages = [s.make_mess() for s in settings]

    catnames, psnr = get_stats_per_category(messages)[0:2]
    catnames = CATEGORIES
    x_axis = [0,2,4,6,8,10]
    fig = plt.figure(figsize=(6,4),dpi=300)
    ax = fig.add_subplot(1,1,1)
    for j in range(psnr.shape[1]):
        ax.plot(x_axis, psnr[:,j], marker=".",markersize=10, linewidth = 2, c = color_blind[j])
    ax.set_xlabel(r'number of luminance bins')
    ax.set_ylabel(r"PSNR (dB)")
    ax.set_ylim(30,50)
    ax.set_yticks([30,35,40,45,50])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.legend(catnames, loc = 'upper right', bbox_to_anchor=(1,.98), ncol =2, frameon = False)

    plt.savefig(outpath, transparent = True, bbox_inches='tight', pad_inches=0.05)
    plt.clf()


def psnr_vs_input_compression(outpath):
    settings = [
        # Settings(128 , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
        # Settings(64  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
        # Settings(32  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,


        Settings(128 , 8 , 85  , 6  , True  , True  , True  , True  , True)  , # Low
        Settings( 64 , 8 , 85  , 6  , True  , True  , True  , True  , True)  ,
        Settings( 32 , 8 , 85  , 6  , True  , True  , True  , True  , True)  ,

        Settings(128 , 4 , 75  , 6  , True  , True  , True  , True  , True)  ,
        Settings( 64 , 4 , 75  , 6  , True  , True  , True  , True  , True)  , # Medium
        Settings( 32 , 4 , 75  , 6  , True  , True  , True  , True  , True)  ,

        Settings(128 , 2 , 60  , 6  , True  , True  , True  , True  , True)  ,
        Settings( 64 , 2 , 60  , 6  , True  , True  , True  , True  , True)  ,
        Settings( 32 , 2 , 60  , 6  , True  , True  , True  , True  , True)  , # High

        SettingsJPEG(8,85,"jpeg")  ,
        SettingsJPEG(4,75,"jpeg")  ,
        SettingsJPEG(2,60,"jpeg")  ,

        SettingsJPEG(8,85,"jpeg_diff")  ,
        SettingsJPEG(4,75,"jpeg_diff")  ,
        SettingsJPEG(2,60,"jpeg_diff")  ,
    ]

    messages = [s.make_mess() for s in settings]

    catnames, psnr,ssim, i_comp,o_comp, count, exp_ids = get_stats_per_category(messages)[0:7]
    i_comp *= 100
    o_comp *= 100
    psnr = np.mean(psnr,axis = 1)
    i_comp = np.mean(i_comp,axis = 1)
    o_comp = np.mean(o_comp,axis = 1)
    o_comp += i_comp
    o_comp /= 2

    # x_axis = [32,64,128]
    fig = plt.figure(figsize=(3.33,2),dpi=300)
    ax = fig.add_subplot(1,1,1)
    numcurves =len(settings)/3
    for j in range(numcurves):
        xx = o_comp[3*j:3*(j+1)]
        yy = psnr[3*j:3*(j+1)]
        ax.plot(xx, yy, marker=".",markersize=10, linewidth = 2, c = color_blind[j])
    ax.set_xlabel(r'Total data ratio $\frac{1}{2}(\%_{up}+\%_{down})$')
    ax.set_ylabel(r"PSNR (in dB, higher is better)", rotation = 0, position = (0.5,1.1), ha='left', va='center')
    ax.set_ylim(25,40)
    ax.set_xlim(0.0,1.5)
    ax.set_yticks([25,30,35,40])
    ax.set_xticks([0.0,0.5,1.0,1.5])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    leg = [
        # "uncompressed input",
        r"ours, $\%_{up}=0.1$",
        r"ours, $\%_{up}=0.5$",
        r"ours, $\%_{up}=1.0$",
        r"jpeg",
        r"jdiff",
    ]
    ax.legend(leg, loc = 'upper right', bbox_to_anchor=(1.1,.85), ncol =1, frameon = False)


    plt.savefig(outpath, transparent = True, bbox_inches='tight', pad_inches=0.05)
    plt.clf()

if __name__ == '__main__':
    output_dir = "output"
    # psnr_vs_nluma(os.path.join(output_dir,"psnr_vs_nluma.pdf"))
    # psnr_vs_wsize(os.path.join(output_dir,"psnr_vs_wsize.pdf"))
    psnr_vs_input_compression(os.path.join(output_dir,"psnr_vs_input_compression.pdf"))
