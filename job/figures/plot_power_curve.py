import os,sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

#-----------------------------------------------------------------------------------------------------------

from shared import jpeg_color, recipe_color, local_color, color_blind
import numpy as np
import scipy.interpolate as interpolate
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

from scipy.ndimage.filters import gaussian_filter1d as gaussian1d

import pandas
import locale

import csv
output_dir = "output"
data = "data"
outpath = os.path.join(output_dir,"power_curve.pdf")

# Use tex
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('legend', fontsize=7)
plt.rc('font', size=8)

local_data = pandas.read_csv(os.path.join(data,'st_local_01.csv'),
    dtype = np.float32,
)
jpeg_data = pandas.read_csv(os.path.join(data,'st_jpeg_01.csv'),
    dtype = np.float32,
)
recipe_data = pandas.read_csv(os.path.join(data,'st_recipe_01.csv'),
    dtype = np.float32,
)

local_time = local_data['Time (s)']
local_power = local_data['Main Avg Power (W)']
jpeg_time = jpeg_data['Time (s)']
jpeg_power = jpeg_data['Main Avg Power (W)']
recipe_time = recipe_data['Time (s)']
recipe_power = recipe_data['Main Avg Power (W)']

# center time
local_time -= local_time[0]
jpeg_time -= jpeg_time[0]
recipe_time -= recipe_time[0]

# For legend
local_tot_time   = local_time.as_matrix()[-1]
jpeg_tot_time    = jpeg_time.as_matrix()[-1]
recipe_tot_time  = recipe_time.as_matrix()[-1]
local_avg_power  = np.mean(local_power)
jpeg_avg_power   = np.mean(jpeg_power)
recipe_avg_power = np.mean(recipe_power)
local_energy     = local_tot_time*local_avg_power
jpeg_energy      = jpeg_tot_time*jpeg_avg_power
recipe_energy    = recipe_tot_time*recipe_avg_power

sigma = 700
local_smooth = gaussian1d(local_power,sigma)
jpeg_smooth = gaussian1d(jpeg_power,sigma)
recipe_smooth = gaussian1d(recipe_power,sigma)

subsample     = 1000
local_time    = local_time[::subsample]
jpeg_time     = jpeg_time[::subsample]
recipe_time   = recipe_time[::subsample]
local_smooth  = local_smooth[::subsample]
jpeg_smooth   = jpeg_smooth[::subsample]
recipe_smooth = recipe_smooth[::subsample]


# ##########
# #  Time  #
# ##########
#
fig = plt.figure(figsize=(3.33,2),dpi=300)
ax = fig.add_subplot(1,1,1)

local = ax.fill_between(local_time,0, local_smooth, color = local_color)
# local = ax.plot(local_time, local_smooth, marker="",markersize=10, linewidth = 1, c = local_color)
jpeg = ax.plot(jpeg_time,jpeg_smooth, marker="",markersize=10, linewidth = 1, c = jpeg_color)
recipe = ax.plot(recipe_time, recipe_smooth, marker="",markersize=10, linewidth = 1, c = recipe_color)

ax.set_ylim(0,8)
# ax.set_xlim(np.log(100),np.log(32000))

ax.tick_params(labeltop='off') # don't put tick labels at the top
ax.xaxis.tick_bottom()

ax.set_yticks(range(0,9,2))
ax.set_xticks(range(0,41,10))

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')

ax.set_xlabel(r'Time (s)')
ax.set_ylabel(r"Power consumption (W)", rotation = 0, position = (0.5,1.1), ha='left', va='center')

# print local_energy, local_tot_time
# print jpeg_energy, jpeg_tot_time
# print recipe_energy, recipe_tot_time


leg = [
    "local processing\t%d s, %d J" % (local_tot_time, local_energy),
    "jpeg cloud\t%d s, %d J"% (jpeg_tot_time, jpeg_energy),
    "our recipes\t%d s, %d J"% (recipe_tot_time, recipe_energy),
]
local_leg = Rectangle((0, 0), 1, 1, fc=local_color, linewidth = 0)
ax.legend((local_leg, jpeg[0],recipe[0]),leg, loc = 'upper right', bbox_to_anchor=(1.,1.1), ncol =1, frameon = False)

plt.savefig(outpath, transparent = True, bbox_inches='tight', pad_inches=0.05)
plt.clf()
