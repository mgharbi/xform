import os
import subprocess

from shared import Settings
from shared import SettingsJPEG

os.environ["DJANGO_ENV"] = "PROD"

settings = [

    #       (wSize , input_ds , input_q , n_luma , overlap , multi , tone  , noise , upsample  )
    # Effect of luma, multiscale, overlap (and combinations)
    # Settings(32  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
    # Settings(32  , 1 , 100 , 6  , False , True  , True  , True  , True ) ,
    # Settings(32  , 1 , 100 , 6  , True  , False , True  , True  , True ) ,
    # Settings(32  , 1 , 100 , 6  , True  , True  , False , True  , True ) ,
    # Settings(32  , 1 , 100 , 6  , False , False , True  , True  , True ) ,
    # Settings(32  , 1 , 100 , 6  , True  , False , False , True  , True ) ,
    # Settings(32  , 1 , 100 , 6  , False , True  , False , True  , True ) ,
    # Settings(32  , 1 , 100 , 6  , False , False , False , True  , True ) ,

    # Effect of luma, multiscale, overlap (and combinations) on standard
    # Settings(32  , 2 , 60  , 6  , False , True  , True  , True  , True ) ,
    # Settings(32  , 2 , 60  , 6  , True  , False , True  , True  , True ) ,
    # Settings(32  , 2 , 60  , 6  , True  , True  , False , True  , True ) ,
    # Settings(32  , 2 , 60  , 6  , False , False , True  , True  , True ) ,
    # Settings(32  , 2 , 60  , 6  , True  , False , False , True  , True ) ,
    # Settings(32  , 2 , 60  , 6  , False , True  , False , True  , True ) ,
    # Settings(32  , 2 , 60  , 6  , False , False , False , True  , True ) ,

    # Effect of luma, multiscale, overlap (and combinations) on medium
    # Settings(64  , 4 , 75  , 6  , False , True  , True  , True  , True ) ,
    # Settings(64  , 4 , 75  , 6  , True  , False , True  , True  , True ) ,
    # Settings(64  , 4 , 75  , 6  , True  , True  , False , True  , True ) ,
    # Settings(64  , 4 , 75  , 6  , False , False , True  , True  , True ) ,
    # Settings(64  , 4 , 75  , 6  , True  , False , False , True  , True ) ,
    # Settings(64  , 4 , 75  , 6  , False , True  , False , True  , True ) ,
    # Settings(64  , 4 , 75  , 6  , False , False , False , True  , True ) ,

    # Effect of luma bands
    # Settings(32  , 1 , 100 , 2  , True  , True  , True  , True  , True)  ,
    # Settings(32  , 1 , 100 , 4  , True  , True  , True  , True  , True)  ,
    # Settings(32  , 1 , 100 , 8  , True  , True  , True  , True  , True)  ,
    # Settings(32  , 1 , 100 , 10 , True  , True  , True  , True  , True)  ,

    # Effect of wSize
    # Settings(16  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
    # Settings(64  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
    # Settings(128 , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
    # Settings(256 , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,

    # Effect of input quality
    # Settings( 32 , 2 , 60  , 6  , True  , True  , True  , True  , True)  , # High
    # Settings( 32 , 4 , 75  , 6  , True  , True  , True  , True  , True)  ,
    # Settings( 32 , 8 , 85  , 6  , True  , True  , True  , True  , True)  ,
    # Settings( 64 , 2 , 60  , 6  , True  , True  , True  , True  , True)  ,
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , True  , True)  , # Medium
    # Settings( 64 , 8 , 85  , 6  , True  , True  , True  , True  , True)  ,
    # Settings(128 , 2 , 60  , 6  , True  , True  , True  , True  , True)  ,
    # Settings(128 , 4 , 75  , 6  , True  , True  , True  , True  , True)  ,
    # Settings(128 , 8 , 85  , 6  , True  , True  , True  , True  , True)  , # Low

    # Effect of noise and upsample
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , False , True)  ,
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , True  , False) ,
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , False , False) ,

################################################################################
#                        With mutliscale hist transfer                         #
################################################################################


    # Effect of luma, multiscale, overlap (and combinations) on standard
    # Settings(32  , 2 , 60  , 6  , False , True  , True  , True  , True ,True ) ,
    # Settings(32  , 2 , 60  , 6  , True  , False , True  , True  , True ,True ) ,
    # Settings(32  , 2 , 60  , 6  , True  , True  , False , True  , True ,True ) ,
    # Settings(32  , 2 , 60  , 6  , False , False , True  , True  , True ,True ) ,
    # Settings(32  , 2 , 60  , 6  , True  , False , False , True  , True ,True ) ,
    # Settings(32  , 2 , 60  , 6  , False , True  , False , True  , True ,True ) ,
    # Settings(32  , 2 , 60  , 6  , False , False , False , True  , True ,True ) ,

    # Effect of luma, multiscale, overlap (and combinations) on medium
    # Settings(64  , 4 , 75  , 6  , False , True  , True  , True  , True ,True ) ,
    # Settings(64  , 4 , 75  , 6  , True  , False , True  , True  , True ,True ) ,
    # Settings(64  , 4 , 75  , 6  , True  , True  , False , True  , True ,True ) ,
    # Settings(64  , 4 , 75  , 6  , False , False , True  , True  , True ,True ) ,
    # Settings(64  , 4 , 75  , 6  , True  , False , False , True  , True ,True ) ,
    # Settings(64  , 4 , 75  , 6  , False , True  , False , True  , True ,True ) ,
    # Settings(64  , 4 , 75  , 6  , False , False , False , True  , True ,True ) ,

    # Effect of input quality
    # Settings( 32 , 2 , 60  , 6  , True  , True  , True  , True  , True ,True)  , # High
    # Settings( 32 , 4 , 75  , 6  , True  , True  , True  , True  , True ,True)  ,
    # Settings( 32 , 8 , 85  , 6  , True  , True  , True  , True  , True ,True)  ,
    # Settings( 64 , 2 , 60  , 6  , True  , True  , True  , True  , True ,True)  ,
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , True  , True ,True)  , # Medium
    # Settings( 64 , 8 , 85  , 6  , True  , True  , True  , True  , True ,True)  ,
    # Settings(128 , 2 , 60  , 6  , True  , True  , True  , True  , True ,True)  ,
    # Settings(128 , 4 , 75  , 6  , True  , True  , True  , True  , True ,True)  ,
    # Settings(128 , 8 , 85  , 6  , True  , True  , True  , True  , True ,True)  , # Low

    # Effect of noise and upsample
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , False , True, True)  ,
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , True  , False, True) ,
    # Settings( 64 , 4 , 75  , 6  , True  , True  , True  , False , False, True) ,


    # Upsample
    Settings( 32 , 2 , 60  , 6  , True  , True  , True  , True  , False, False) ,
    Settings( 128 , 8 , 85  , 6  , True  , True  , True  , True  , False, False) ,
]

settings_jpeg = [
    # SettingsJPEG(1 , 100 , "jpeg")      ,
    SettingsJPEG(2 , 60  , "jpeg")      ,
    SettingsJPEG(4 , 75  , "jpeg")      ,
    SettingsJPEG(8 , 85  , "jpeg")      ,
    # SettingsJPEG(1 , 100 , "jpeg_diff") ,
    SettingsJPEG(2 , 60  , "jpeg_diff") ,
    SettingsJPEG(4 , 75  , "jpeg_diff") ,
    SettingsJPEG(8 , 85  , "jpeg_diff") ,
]

idx = 0
for s in settings:
    s.run()
    idx += 1

# for s in settings_jpeg:
#     s.run()
#     idx += 1
print "Processed %d experiments" % idx
