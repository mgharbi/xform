import os

cmd = """python evaluate.py LocalLaplacian -f 0003 --limit 1 \
--wSize %d \
--input-downsampling %d \
--input-quality %d \
--luma-bands %d \
--%spatch-overlap \
--%smultiscale-features \
--%stonecurve-features \
--%snoise \
--%supsample \
--%stransfer \
-m '%s'
"""

class Settings(object):
    def __init__(self,wSize, in_ds, i_q, luma_bands, overlap, multiscale,tone,noise,upsample,transfer = False):
        self.w   = wSize
        self.ids = in_ds
        self.iq  = i_q
        self.l   = luma_bands

        if overlap:
            self.p   = ""
        else:
            self.p   = "no-"

        if multiscale:
            self.m   = ""
        else:
            self.m   = "no-"
        if tone:
            self.t   = ""
        else:
            self.t   = "no-"
        if noise:
            self.noise   = ""
        else:
            self.noise   = "no-"
        if upsample:
            self.upsample   = ""
        else:
            self.upsample   = "no-"
        if transfer:
            self.transfer   = ""
        else:
            self.transfer   = "no-"

    def make_mess(self):
        if self.t == "no-":
            message = "ds:%d | q:%3d | w:%3d | %soverlap,\t %smultiscale,\t %stonecurve,\t %snoise,\t %supsample" % (self.ids, self.iq, self.w, self.p, self.m, self.t, self.noise, self.upsample)
        else:
            message = "ds:%d | q:%3d | w:%3d | %soverlap,\t %smultiscale,\t %stonecurve (%d),\t %snoise,\t %supsample" % (self.ids, self.iq, self.w, self.p, self.m, self.t, self.l, self.noise, self.upsample)
        if not self.transfer:
            message += "\t transfer"
        return message

    def make_cmd(self):
        return cmd % (self.w, self.ids, self.iq, self.l, self.p, self.m, self.t,self.noise,self.upsample, self.transfer, self.make_mess())

    def run(self):
        cmd = self.make_cmd()
        os.system(cmd)

