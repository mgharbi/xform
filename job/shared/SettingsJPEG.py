import os

cmd = """python evaluate.py LocalLaplacian -f 0003 --limit 1 \
--input-downsampling %d \
--input-quality %d \
--mode %s \
-m '%s'
"""

class SettingsJPEG(object):
    def __init__(self, in_ds, i_q, mode):
        self.ids = in_ds
        self.iq  = i_q
        self.mode = mode

    def make_mess(self):
        message = "ds:%d | q:%3d | %s" % (self.ids, self.iq, self.mode)
        return message

    def make_cmd(self):
        return cmd % (self.ids, self.iq, self.mode, self.make_mess())

    def run(self):
        cmd = self.make_cmd()
        os.system(cmd)

