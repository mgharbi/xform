color_blind = [
    (255,128,14),
    (171,171,171),
    (95,158,209),
    (89,89,89),
    (0,107,164),
    (255,188,121),
    (207,207,207),
    (200,82,0),
    (162,200,236),
    (137,137,137),
]
for i in range(len(color_blind)):
    r, g, b = color_blind[i]
    color_blind[i] = (r / 255., g / 255., b / 255.)

jpeg_color = color_blind[2]
recipe_color = color_blind[0]
local_color = color_blind[6]

