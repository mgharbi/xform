import sys, os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir, "transform_compression"))

os.environ["DJANGO_ENV"] = "PROD"

from settings import *

# Django database connection
sys.path = sys.path + [os.path.join(BASE_DIR,"analysis")]
os.environ.setdefault("DJANGO_SETTINGS_MODULE","analysis.settings")
from summary.models import Result, Experiment, Category, Parameters

#-----------------------------------------------------------------------------------------------------------

import numpy as np


def metrics(result_list):
    n = len(result_list)
    ret = np.zeros((n,4))
    for i,r in enumerate(result_list):
        ret[i,0] = r.psnr
        ret[i,1] = r.ssim
        ret[i,2] = r.compression_up
        ret[i,3] = r.compression_down
    return ret

def get_experiments_from_messages(messages):
    nm = len(messages)
    exp_ids = np.zeros((nm,))
    for im,m in enumerate(messages):
        print m
        e = Experiment.objects.all().filter(description = m)[0]
        exp_ids[im] = e.id
    return exp_ids

def get_stats_per_category(messages = None, ids = None):
    """ category in columns, experiment in rows"""
    catnames = []
    empty = []
    cats = Category.objects.all()

    if messages is not None:
        experiment_ids = get_experiments_from_messages(messages)
    else:
        experiment_ids = ids

    nc = len(cats)
    nm = len(experiment_ids)

    psnr               = np.zeros((nm,nc))
    ssim               = np.zeros((nm,nc))
    input_compression  = np.zeros((nm,nc))
    output_compression = np.zeros((nm,nc))
    count              = np.zeros((nm,nc))


    for ic, c in enumerate(cats):
        n = 0
        for im,eid in enumerate(experiment_ids):
            e = Experiment.objects.all().filter(id = eid)[0]
            r = Result.objects.all().filter(experiment = e, category = c)
            n += len(r)
            if len(r) > 0:
                vals                      = metrics(r)
                vals                      = np.mean(vals,axis = 0)
                psnr[im,ic]               = vals[0]
                ssim[im,ic]               = vals[1]
                input_compression[im,ic]  = vals[2]
                output_compression[im,ic] = vals[3]
                count[im,ic]              = len(r)
        if n == 0:
            empty.append(ic)
        else:
            catnames.append(c.name)
    psnr = np.delete(psnr,empty, axis = 1)
    ssim = np.delete(ssim,empty, axis = 1)
    input_compression = np.delete(input_compression,empty, axis = 1)
    output_compression = np.delete(output_compression,empty, axis = 1)
    count = np.delete(count,empty, axis = 1)
    return catnames,psnr,ssim,input_compression,output_compression,count,experiment_ids


