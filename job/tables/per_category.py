import os,sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

#-----------------------------------------------------------------------------------------------------------

from shared import Settings, color_blind,get_stats_per_category, SettingsJPEG
import numpy as np
import matplotlib.pyplot as plt

settings = [
    Settings(32  , 1 , 100 , 6  , True  , True  , True  , True  , True ) ,
    SettingsJPEG(1 , 100 , "jpeg")      ,
    SettingsJPEG(1 , 100 , "jpeg_diff")      ,

    Settings( 32 , 2 , 60  , 6  , True  , True  , True  , True  , True)  , # High
    SettingsJPEG(2 , 60  , "jpeg")      ,
    SettingsJPEG(2 , 60  , "jpeg_diff")      ,

    Settings( 64 , 4 , 75  , 6  , True  , True  , True  , True  , True)  , # Medium
    SettingsJPEG(4 , 75  , "jpeg")      ,
    SettingsJPEG(4 , 75  , "jpeg_diff")      ,

    Settings(128 , 8 , 85  , 6  , True  , True  , True  , True  , True)  , # Low
    SettingsJPEG(8 , 85  , "jpeg")      ,
    SettingsJPEG(8 , 85  , "jpeg_diff")      ,
]

messages = [s.make_mess() for s in settings]

catnames, psnr,ssim, in_comp, out_comp, count = get_stats_per_category(messages)[0:6]

in_comp *= 100
out_comp *= 100

SEP = " & "
ENDL = r" \\"
def print_line(cat,count,in_comp, out_comps, psnrs, ssims):
    line = ""
    line += SEP + cat
    line += SEP + ( "%d" % count)
    line += SEP + ("%.1f" % in_comp)
    line += SEP

    for v,val in enumerate(out_comps):
        line += SEP
        line += ("%.1f" % val)
    line += SEP

    best = np.argmax(psnrs)
    for v,val in enumerate(psnrs):
        line += SEP
        if v == best:
            line += ("\\textbf{%.1f}" % val)
        else:
            line += ("%.1f" % val)
    line += SEP

    best = np.argmax(ssims)
    for v,val in enumerate(ssims):
        line += SEP
        if v == best:
            line += ("\\textbf{%.2f}" % val)
        else:
            line += ("%.2f" % val)
    line += ENDL
    return line

outpath = "table.tex"
f = open(outpath, "w")

n_qual = len(settings)/3
label = ["non-degraded input", "standard", "medium", "low"]
for q in range(n_qual):
    m_psnr = np.mean(psnr[3*q:3*(q+1),:], axis=1)
    m_ssim = np.mean(ssim[3*q:3*(q+1),:], axis=1)
    m_in_c = np.mean(in_comp[3*q:3*(q+1),:], axis=1)
    m_out_c = np.mean(out_comp[3*q:3*(q+1),:], axis=1)
    n_sum = np.sum(count[3*q:3*(q+1),:], axis=1)

    f.write(label[q])
    for ic in range(len(catnames)):
        line = print_line(catnames[ic],
                          count[3*q,ic],
                          in_comp[3*q,ic],
                          out_comp[3*q:3*(q+1),ic],
                          psnr[3*q:3*(q+1),ic],
                          ssim[3*q:3*(q+1),ic],
                          )
        f.write(line)
        f.write("\n")

    f.write(r"\rule{0pt}{4ex}")
    f.write("\n")
    line = print_line("all",
        n_sum[0],
        m_in_c[0],
        m_out_c,
        m_psnr,
        m_ssim )
    f.write(line)
    f.write("\n")
    f.write("\n")
