import os,sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

#-----------------------------------------------------------------------------------------------------------

from shared import Settings, color_blind,get_stats_per_category, SettingsJPEG
import numpy as np
import matplotlib.pyplot as plt

import argparse

def main():
    upsample = True

    settings = [
        Settings(32  , 2 , 60 , 6  , True  , True  , True  , True  , False ) ,
        Settings(32  , 2 , 60 , 6  , True  , True  , True  , True  , True ) ,
        Settings(64  , 4 , 75 , 6  , True  , True  , True  , True  , False ) ,
        Settings(64  , 4 , 75 , 6  , True  , True  , True  , True  , True ) ,
        Settings(128  , 8 , 85 , 6  , True  , True  , True  , True  , False ) ,
        Settings(128  , 8 , 85 , 6  , True  , True  , True  , True  , True ) ,
        #
        # Settings(128  , 8 , 85 , 6  , True  , True  , True  , True  , upsample ) ,
        # Settings(128  , 8 , 85 , 6  , True  , True  , True  , True  , True ) ,
    ]

    messages = [s.make_mess() for s in settings]

    catnames, psnr,ssim, in_comp, out_comp, count = get_stats_per_category(messages)[0:6]
    in_comp  *= 100
    out_comp *= 100

    catnames.append("all")

    # Compute mean across cats
    m_out_c = np.mean(out_comp, axis=1)
    m_out_c.shape += (1,)
    out_comp = np.concatenate((out_comp,m_out_c), axis = 1)

    m_psnr = np.mean(psnr, axis=1)
    m_psnr.shape += (1,)
    psnr = np.concatenate((psnr,m_psnr), axis = 1)

    m_ssim = np.mean(ssim, axis=1)
    m_ssim.shape += (1,)
    ssim = np.concatenate((ssim,m_ssim), axis = 1)

    nexp = psnr.shape[0]
    ncat = len(catnames)

    SEP = " & "
    ENDL = r" \\"

    outpath = "upsample_table.tex"
    f = open(outpath, "w")

    for ic in range(ncat):
        line = ""
        if ic == 0:
            line += "$\%_{down}$"
        if ic == ncat-1:
            f.write("\\rule{0pt}{4ex}\n")

        line += SEP + catnames[ic]
        line += SEP + " "
        for ie in range(nexp):
            line += SEP
            line += "%.1f" % out_comp[ie,ic]
        line += ENDL
        f.write(line)
        f.write("\n")

    f.write("\\rule{0pt}{4ex}\n\n")

    for ic in range(ncat):
        line = ""
        if ic == 0:
            line += "\\psnr (\\db)"
        if ic == ncat-1:
            f.write("\\rule{0pt}{4ex}\n")

        line += SEP + catnames[ic]
        line += SEP + " "
        for ie in range(nexp):
            line += SEP
            line += "%.1f" % psnr[ie,ic]
        line += ENDL
        f.write(line)
        f.write("\n")

    f.write("\\rule{0pt}{4ex}\n\n")

    for ic in range(ncat):
        line = ""
        if ic == 0:
            line += "\\ssim"
        if ic == ncat-1:
            f.write("\\rule{0pt}{4ex}\n")

        line += SEP + catnames[ic]
        line += SEP + " "
        for ie in range(nexp):
            line += SEP
            line += "%.2f" % ssim[ie,ic]
        line += ENDL
        f.write(line)
        f.write("\n")

    f.close()

if __name__ == '__main__':
    main()

