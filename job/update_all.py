import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("exp_id", type=int, nargs='*', default = -1)
parser.add_argument("--start", type=int, default = 1)
args = parser.parse_args()

os.environ["DJANGO_ENV"] = "PROD"

if args.exp_id == -1:
    for i in range(args.start,100):
        os.system('python evaluate.py all --update %d' % i)
else:
    for i in args.exp_id:
        os.system('python evaluate.py all --update %d' % i)


