# ----------------------------------------------------------------------------
# File:    make_webpage.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2015-02-11
# ----------------------------------------------------------------------------
#
# Generate static website for supplemental material
#
# ---------------------------------------------------------------------------#


import os, sys, math, shutil
import argparse
import numpy as np
from PIL import Image

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..","..", "transform_compression"))

from settings import *

#-----------------------------------------------------------------------------------------------------------

# Django database connection
sys.path = sys.path + [os.path.join(BASE_DIR,"analysis")]
if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE","analysis.settings")
from summary.models import Result, Experiment, Category, Parameters

# from django.template import Template, Context, loader
import jinja2
from jinja2 import Template

#-----------------------------------------------------------------------------------------------------------

WEB_SRC_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)),"web_src")
WEB_DIR = "/Users/mgharbi/Documents/xform_web/"
if not os.path.exists(WEB_DIR):
    os.makedirs(WEB_DIR)
LOADER = jinja2.loaders.FileSystemLoader(WEB_SRC_DIR)
ENV = jinja2.Environment(loader=LOADER)

quality_suffix = [
    "_low.jpg",
    "_medium.jpg",
    "_high.jpg",
]

def resize(i_path,o_path,width):
    I = Image.open(i_path)
    ratio = (width/float(I.size[0]))
    height = int(ratio*I.size[1])
    O = I.resize((width, height), Image.ANTIALIAS)
    O.save(o_path)

def copy_image(i_path,o_path):
    I = Image.open(i_path)
    I.save(o_path)

def copy_static_files():
    files = [
        "static",
    ]
    for f in files:
        src = os.path.join(WEB_SRC_DIR, f)
        target = os.path.join(WEB_DIR, f)
        if os.path.isdir(src):
            if os.path.exists(target):
                shutil.rmtree(target)
            shutil.copytree(src, target)
        else:
            if os.path.exists(target):
                os.remove(target)
            shutil.copy(src, target)

def make_index_page(cats, exp):
    print "  + Making index page"
    t = ENV.get_template("index.html")
    f = open(os.path.join(WEB_DIR,"index.html"),'w')
    n_cats = 10
    n_imgs = 168
    f.write(t.render(n_cats = n_cats, n_imgs = n_imgs))
    f.close()

def make_result_page(exp, cats, metrics):
    print "  + Making result page"
    t = ENV.get_template("results.html")
    f = open(os.path.join(WEB_DIR,"results.html"),'w')
    f.write(t.render(category=cats, metrics = metrics))
    f.close()

def make_crop_view_page(exp, category, results):
    t = ENV.get_template("crop_view.html")
    recipe_exp = exp[0:3]
    jpeg_exp = exp[3:6]
    for r in results:
        resultdir = os.path.join(WEB_DIR,"results",category.name.lower())
        if not os.path.exists(resultdir):
            os.makedirs(resultdir)
        f = open(os.path.join(resultdir,r.name+".html"),'w')
        f.write(t.render(jpeg_exp = jpeg_exp,
            recipe_exp = recipe_exp,
            quality_suffix = quality_suffix,
            c=category,
            low = r.low,
            medium = r.medium,
            low_jpg = r.low_jpg,
            medium_jpg = r.medium_jpg,
            high_jpg = r.high_jpg,
            high = r.high))
        f.close()

class ResultDisplay(object):
    def __init__(self, rr,exp):
        self.name = rr[0].name
        self.comp = []
        self.psnr = []
        self.ssim = []
        for ie,e in enumerate(exp):
            r = rr.filter(experiment = e)[0]
            self.comp.append(r.compression_down)
            self.psnr.append(r.psnr)
            self.ssim.append(r.ssim)
            if ie == 0:
                self.low = r
            elif ie == 1:
                self.medium = r
            elif ie == 2:
                self.high = r
            elif ie == 3:
                self.low_jpg = r
            elif ie == 4:
                self.medium_jpg = r
            elif ie == 5:
                self.high_jpg = r



def prepare_result_list(exp, c):
    results = Result.objects.extra().filter(experiment__in = exp, category = c)
    names = {r.name for r in results}
    result_list = []
    for n in names:
        rr = results.filter(name = n)
        result_list.append(ResultDisplay(rr,exp))
    return result_list

def make_category_pages(exp, cats):
    print "  + Making category pages"
    t = ENV.get_template("category.html")
    resultdir = os.path.join(WEB_DIR,"results")
    if not os.path.exists(resultdir):
        os.makedirs(resultdir)
    for c in cats:
        print "    - %s" % c.name
        results = prepare_result_list(exp,c)

        # Html for the category
        f = open(os.path.join(resultdir,c.name.lower()+".html"),'w')
        catdir = os.path.join(resultdir, c.name.lower())
        f.write(t.render(category=c,
                        results=results,
        ))
        f.close()

        # Folder for the individual results
        if not os.path.exists(catdir):
            os.makedirs(catdir)
        make_crop_view_page(exp, c, results)

def check_exp(exp):
    if len(exp)<6:
        return False
    for i in range(0,3):
        if not exp[i].parameters.transform_model == "RecipeModel":
            return False
        # print "Recipe: i_down: %d" % exp[i].parameters.input_ratio
    for i in range(3,6):
        if not exp[i].parameters.transform_model == "JPEGmodel":
            return False
        # print "JPEG: i_down: %d" % exp[i].parameters.input_ratio
    return True

def aggregate_metrics(exp, cats):
    ne = len(exp)/2
    nc = len(cats)
    comp = np.zeros((nc,ne))
    psnr = np.zeros((nc,ne))
    ssim = np.zeros((nc,ne))
    n_examples = np.zeros((nc,ne))
    for ic,c in enumerate(cats):
        for ie in range(ne):
            res = Result.objects.all().filter(experiment = exp[ie], category = c)
            for r in res:
                comp[ic,ie] += (r.compression_down)
                psnr[ic,ie] += r.psnr
                ssim[ic,ie] += r.ssim
                n_examples[ic,ie] += 1
    print n_examples
    comp /= n_examples
    psnr /= n_examples
    ssim /= n_examples
    return (comp,psnr,ssim,n_examples)

def copy_inputs(cats):
    print "  + copying input data"
    # for c in cats:
    #     print "    - %s" % c.name
    #     src = os.path.join(DATA_DIR,c.name)
    #     target = os.path.join(WEB_DIR,"images","input", c.name)
    #     if os.path.exists(target):
    #         shutil.rmtree(target)
    #     shutil.copytree(src,target)
    # print "  + creating thumbnails"
    for c in cats:
        print "    - %s" % c.name
        src = os.path.join(DATA_DIR,c.name)
        target = os.path.join(WEB_DIR,"images","input", c.name)
        for f in os.listdir(src):
            path = os.path.join(src,f)
            path_target = os.path.join(target,f)
            if not os.path.exists(path_target):
                os.makedirs(path_target)
            print path
            print path_target
            if os.path.isdir(path):
                src_file = os.path.join(path,"unprocessed.png")
                dst_file = os.path.join(path_target,"unprocessed.jpg")
                copy_image(src_file,dst_file)
                src_file = os.path.join(path,"processed.png")
                dst_file = os.path.join(path_target,"processed.jpg")
                copy_image(src_file,dst_file)

                src_file = os.path.join(path,"unprocessed_us_02_jpeg_60.png")
                dst_file = os.path.join(path_target,"unprocessed_high.jpg")
                copy_image(src_file,dst_file)
                src_file = os.path.join(path,"processed_us_02_jpeg_60.png")
                dst_file = os.path.join(path_target,"processed_high.jpg")
                copy_image(src_file,dst_file)

                src_file = os.path.join(path,"unprocessed_us_04_jpeg_75.png")
                dst_file = os.path.join(path_target,"unprocessed_medium.jpg")
                copy_image(src_file,dst_file)
                src_file = os.path.join(path,"processed_us_04_jpeg_75.png")
                dst_file = os.path.join(path_target,"processed_medium.jpg")
                copy_image(src_file,dst_file)

                src_file = os.path.join(path,"unprocessed_us_08_jpeg_85.png")
                dst_file = os.path.join(path_target,"unprocessed_low.jpg")
                copy_image(src_file,dst_file)
                src_file = os.path.join(path,"processed_us_08_jpeg_85.png")
                dst_file = os.path.join(path_target,"processed_low.jpg")
                copy_image(src_file,dst_file)

def copy_results(exp):
    print "  + copying output data"
    for e in exp:
        print "    - experiment %d" % e.id
        src = os.path.join(OUTPUT_DIR,"%d" % e.id)
        target = os.path.join(WEB_DIR,"images","%d" % e.id)
        if os.path.exists(target):
            shutil.rmtree(target)
        shutil.copytree(src,target)

def make_small_images(exp, cats):
    print "  + creating thumbnails"
    for c in cats:
        print "    - %s" % c.name
        cat_dir = os.path.join(WEB_DIR,"images", "input",c.name)
        for f in os.listdir(cat_dir):
            path = os.path.join(cat_dir,f)
            if ".DS_Store" in path:
                continue
            if os.path.isdir(path):
                src = os.path.join(path,"unprocessed.jpg")
                dst = os.path.join(path,"unprocessed_small.jpg")
                resize(src,dst,390)
                src = os.path.join(path,"processed.jpg")
                dst = os.path.join(path,"processed_small.jpg")
                resize(src,dst,390)
    # for e in exp[0:3]:
    #     print "    - experiment %d" % e.id
    #     exp_dir = os.path.join(WEB_DIR,"images","%d" % e.id)
    #     for c in cats:
    #         cat_dir = os.path.join(exp_dir,c.name)
    #         for f in os.listdir(cat_dir):
    #             path = os.path.join(cat_dir,f)
    #             if not os.path.isdir(path):
    #                 continue
    #             if os.path.isdir(path):
    #                 src = os.path.join(path,"reconstructed.jpg")
    #                 dst = os.path.join(path,"reconstructed_small.jpg")
    #                 resize(src,dst,390)
    #
    #                 # remove recipe
    #                 os.remove(os.path.join(path,"compressed_model_hp.png"))
    #                 os.remove(os.path.join(path,"compressed_model_lp.tif"))

def make_web_page(exp_ids):
    if not os.path.exists(WEB_DIR):
        os.makedirs(WEB_DIR)

    print "Building static website %s -> %s" % (WEB_SRC_DIR, WEB_DIR)

    exp = [Experiment.objects.get(id = i) for i in exp_ids]
    if not check_exp(exp):
        print "  - Error, format is wrong: recipe recipe recipe jpeg jpeg jpeg "
        return
    cats = Category.objects.all()
    # cats = [c for c in cats if not ("_" in c.name)]

    # copy_static_files()
    # metrics = aggregate_metrics(exp,cats)
    make_index_page(cats, exp)
    # make_result_page(exp, cats, metrics)
    # make_category_pages(exp, cats)

    # File processing
    # copy_inputs(cats)
    # copy_results(exp)
    # make_small_images(exp,cats)

if __name__ == '__main__':
    queries = ["metrics"]
    parser = argparse.ArgumentParser("Make webpage format is low mid high, recipe, jpeg")
    parser.add_argument('exp_ids', type=int, nargs ='*')
    args = parser.parse_args()

    make_web_page(args.exp_ids)
