$(document).ready(function() {
    var img = $("img#flip");

    $("#flip_jpg").click(function(event){
        event.preventDefault();
    });
    $("#flip_ref").click(function(event){
        event.preventDefault();
    });
    $("#flip_in").click(function(event){
        event.preventDefault();
    });
    $("#flip_in_degraded").click(function(event){
        event.preventDefault();
    });
    $("#flip_ours").click(function(event){
        event.preventDefault();
    });
    var flip_in = $("#flip_in");
    flip_in.on("mouseover", function() {
        img.attr("src", global_path.input);
        $("#alternate_title").text("Displaying: full quality input")
    });
    var flip_in_degraded = $("#flip_in_degraded");
    flip_in_degraded.on("mouseover", function() {
        img.attr("src", paths[quality].degraded_input);
        $("#alternate_title").text("Displaying: degraded input")
    });
    var flip_jpg = $("#flip_jpg");
    flip_jpg.on("mouseover", function() {
        img.attr("src", paths[quality].jpeg);
        $("#alternate_title").text("Displaying: degraded output")
    });
    var flip_ours = $("#flip_ours");
    flip_ours.on("mouseover", function() {
        img.attr("src", paths[quality].recipe);
        $("#alternate_title").text("Displaying: our reconstruction")
    });
    var flip_ref = $("#flip_ref");
    flip_ref.on("mouseover", function() {
        img.attr("src", global_path.output);
        $("#alternate_title").text("Displaying: ground truth output")
    });
});
