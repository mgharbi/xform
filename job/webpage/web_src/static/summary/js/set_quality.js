$(document).ready(function() {
    var evt = new Event();
    var m;
    var p = paths[quality];

    $(".medium_quality").addClass("quality-selected");

    var set_quality = function() {
        var p = paths[quality];
        // Update images
        $("#input_ref").attr("src", global_path.input_small);
        $("#output_thumb").attr("src", global_path.output_small);
        $("#reconstruct_thumb").attr("src", p.recipe_small);
        $("#flip").attr("src", global_path.input);
        $("#alternate_title").text("Displaying: full quality input")
        
        // Update links
        $("#link_input").attr("href", global_path.input);
        $("#link_degraded_input").attr("href", p.degraded_input);
        $("#link_reference_output").attr("href", global_path.output);
        $("#link_recipe").attr("href", p.recipe);
        $("#link_jpeg").attr("href", p.jpeg);

        // Update metrics
        $(".psnr").text(psnr[quality]+"dB");
        $(".data_up").text(c_in[quality]+"%");
        $(".data_down").text(c_out[quality]+"%");
        $(".ssim").text(ssim[quality]);
        $(".psnr_jpg").text(psnr[3+quality]+"dB");
        $(".data_up_jpg").text(c_in[3+quality]+"%");
        $(".data_down_jpg").text(c_out[3+quality]+"%");
        $(".ssim_jpg").text(ssim[3+quality]);
        
        // cleanup the preview
        var preview = $(".magnifier-preview");
        preview.empty();

        m = new Magnifier(evt);
        m.attach({
            thumb: '#output_thumb',
            large: [
                global_path.input,
                p.degraded_input,
                p.jpeg,
                p.recipe,
                global_path.output,
            ],
            largeWrapper: ['input','degraded_input', 'jpeg', 'ours','output' ],
            zoomable: true,
            zoom: 10
        });
        m.attach({
            thumb: '#reconstruct_thumb',
            large: [
                global_path.input,
                p.degraded_input,
                p.jpeg,
                p.recipe,
                global_path.output,
            ],
            largeWrapper: ['input','degraded_input', 'jpeg', 'ours', 'output'],
            zoomable: true,
            zoom: 10
        });
    };
    console.log($(".low_quality"));
     $(".low_quality").click(function(event){
         $(".low_quality").addClass("quality-selected");
         $(".medium_quality").removeClass("quality-selected");
         $(".high_quality").removeClass("quality-selected");
         event.preventDefault();
         quality = 0;
         set_quality();
     });
     $(".medium_quality").click(function(event){
         console.log("ADD");
         $(".low_quality").removeClass("quality-selected");
         $(".medium_quality").addClass("quality-selected");
         $(".high_quality").removeClass("quality-selected");
         event.preventDefault();
         quality = 1;
         set_quality();
     });
     $(".high_quality").click(function(event){
         $(".low_quality").removeClass("quality-selected");
         $(".medium_quality").removeClass("quality-selected");
         $(".high_quality").addClass("quality-selected");
         event.preventDefault();
         quality = 2;
         set_quality();
     });
    set_quality();
});
