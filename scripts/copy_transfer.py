# -------------------------------------------------------------------
# File:    multiscale_input_transfer.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2014-14-05
# -------------------------------------------------------------------
#
# Generate degraded images for input compression.
#
# ------------------------------------------------------------------#

import os,sys
import re
import shutil
import argparse
import json

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "transform_compression"))
from settings import *
import utils.filesystem as fs

import input_transfer as xfer

def main(category):
    dataDir = DATA_DIR
    ext     = ".png"

    params = [
        {
            "ratio": 2,
            "quality": 60,
        },
        {
            "ratio": 4,
            "quality": 75,
        },
        {
            "ratio": 8,
            "quality": 85,
        },
    ]

    catDir = os.path.join(dataDir,category)
    if not os.path.isdir(catDir):
        return
    for img in os.listdir(catDir):
        imDir = os.path.join(catDir,img)
        if not os.path.isdir(imDir):
            continue

        ref_path = os.path.join(imDir,"unprocessed.png")
        if not os.path.exists(ref_path):
            print "no reference unprocessed, skipping %s" % img
            continue

        print "processing %s %s" % (category, img)

        for p in params:
            ratio   = p["ratio"]
            quality = p["quality"]

            for name in ["processed","unprocessed"]:
                for extension in [".png",".json"]:
                    fname    = os.path.join(imDir,"%s_%s_%02d_jpeg_%s%s%s" % (name, "%s",ratio,"%02d", "%s","%s"))
                    fname    = fname % ("us",quality,"_noise",extension)
                    fnew,ext = os.path.splitext(fname)
                    fnew     = fnew + "_transfer" + ext

                    if not os.path.exists(fname):
                        continue
                    if not os.path.exists(fnew):
                        print fname, fnew
                        shutil.copy(fname, fnew)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("category")
    args = parser.parse_args()
    print "Generating degraded inputs in %s" % args.category
    if args.category == 'all':
        allCat = fs.listSubdir(DATA_DIR)
        for c in allCat:
            main(c)
    main(args.category)
