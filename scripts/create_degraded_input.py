# -------------------------------------------------------------------
# File:    create_degraded_input.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2014-11-05
# -------------------------------------------------------------------
#
# Generate degraded images for input compression.
#
# ------------------------------------------------------------------#

import os,sys
import re
import shutil
import argparse
import json
import numpy as np

import numpy as np

from PIL import Image

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "transform_compression"))
from settings import *
import utils.filesystem as fs
from mghimproc import imresize, save_and_get_img

noDownsample = [
    "PortraitTransfer",
    "Photoshop",
    "TimeOfDay",
]

def add_noise(I):
    dtype = I.dtype
    sigma = 1
    sz = I.shape
    I = I.astype(np.float32)
    I = I+sigma*np.random.randn(sz[0],sz[1],sz[2])
    I[I<0] = 0
    I[I>255] = 255
    return I.astype(dtype)

def dump_json(ff, fsize):
    ff,ext = os.path.splitext(ff)
    ff += ".json"
    f = open(ff,'w')
    f.write(json.dumps({"fsize": fsize}))
    f.close()

def main(category):
    dataDir = DATA_DIR
    ext     = ".png"

    params = [
        {
            "ratio": 2,
            "quality": 60,
        },
        {
            "ratio": 4,
            "quality": 75,
        },
        {
            "ratio": 8,
            "quality": 85,
        },
    ]

    for cat in os.listdir(dataDir):
        if category != "" and cat != category:
            continue
        catDir = os.path.join(dataDir,cat)
        if not os.path.isdir(catDir):
            continue
        for img in os.listdir(catDir):
            imDir = os.path.join(catDir,img)
            if not os.path.isdir(imDir):
                continue
            for f in os.listdir(imDir):
                match = re.match("unprocessed"+ext, f)
                if not match:
                    continue
                print "processing %s" % img

                im_path = os.path.join(imDir,f)
                im_file = Image.open(im_path)
                I       = np.squeeze(np.array(im_file))
                for p in params:
                    ratio = p["ratio"]
                    quality = p["quality"]

                    # Create downsampled image
                    Inew = Image.fromarray(I).resize((I.shape[1]/ratio,I.shape[0]/ratio), Image.LANCZOS)
                    Inew = np.array(Inew)

                    fname = os.path.join(imDir,"unprocessed_%s_%02d_jpeg_%s%s%s" % ("%s",ratio,"%02d", "%s","%s"))

                    # Create jpeg degraded image
                    o_ext = ".jpg"

                    # Downsampled
                    fsmall = fname % ("ds", quality,"", o_ext)
                    im, fsize = save_and_get_img(Inew,fsmall, quality = quality, subsampling = 0)
                    os.remove(fsmall)
                    if not category in noDownsample:
                        fsmall = fname % ("ds", quality,"", ".png")
                        save_and_get_img(im,fsmall)
                        dump_json(fsmall, fsize)

                    # Downsampled+noise
                    if not category in noDownsample:
                        fsmall_noise = fname % ("ds",quality,"_noise", ".png")
                        save_and_get_img(add_noise(im),fsmall_noise)
                        dump_json(fsmall_noise, fsize)

                    # upsample the original jpeg
                    im = np.array(Image.fromarray(im).resize((I.shape[1],I.shape[0]), Image.LANCZOS))

                    # Upsampled
                    f_up = fname % ("us",quality,"",".png")
                    save_and_get_img(im,f_up)
                    dump_json(f_up, fsize)

                    # Upsampled+noise
                    f_up_noise = fname % ("us",quality,"_noise",".png")
                    save_and_get_img(add_noise(im),f_up_noise)
                    dump_json(f_up_noise, fsize)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("category")
    args = parser.parse_args()
    print "Generating degraded inputs in %s" % args.category
    if args.category == 'all':
        allCat = fs.listSubdir(DATA_DIR)
        for c in allCat:
            main(c)
    main(args.category)
