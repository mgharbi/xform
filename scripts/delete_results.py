import os, sys, math
import argparse
import numpy as np

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "transform_compression"))

from settings import *

#-----------------------------------------------------------------------------------------------------------

# Django database connection
sys.path = sys.path + [os.path.join(BASE_DIR,"analysis")]
if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE","analysis.settings")
from summary.models import Result, Experiment, Category, Parameters

#-----------------------------------------------------------------------------------------------------------

def main(args):
    e = args.experiment
    c = args.category
    r = args.result

    if e is not None:
        exp = Experiment.objects.all().filter(id__in = e)
        if c is not None:
            cat = Category.objects.all().filter(name__in = c, experiment__in = exp)
            if r is not None:
                res = Result.objects.all().filter(name__in = r, experiment__in = exp, category__in = cat)
                confirm = raw_input('Delete RESULT %s of category "%s" in experiment %d ? (y/n): ' % (r,c,e))
                if confirm == "y":
                    for rr in res:
                        print "  - deleting",rr
                        rr.delete()
                else:
                    return
            else:
                res = Result.objects.all().filter(experiment__in = exp, category__in = cat)
                confirm = raw_input('Delete CATEGORY "%s" in experiment %s ? (y/n): ' % (c,e))
                if confirm == "y":
                    for rr in res:
                        print "  - deleting",rr
                        rr.delete()
                else:
                    return
        else: # no category defined
                confirm = raw_input('Delete %d EXPERIMENT %s? (y/n): ' % (len(exp),e))
                if confirm == "y":
                    for ee in exp:
                        print "  - deleting", ee
                        ee.delete()
                else:
                    return
    else: # no experiment defined
        if c is not None:
            cat = Category.objects.all().filter(name__in = c)
            if r is not None:
                res = Result.objects.all().filter(name__in = r, category__in = cat)
                confirm = raw_input('Delete RESULT %s of category "%s" in all experiments? (y/n): ' % (r,c))
                if confirm == "y":
                    for rr in res:
                        print "  - deleting",rr
                        rr.delete()
                else:
                    return
            else:
                confirm = raw_input('Delete CATEGORY "%s" in all experiments? (y/n): ' % c)
                res = Result.objects.all().filter(category__in = cat)
                if confirm == "y":
                    for rr in res:
                        print "  - deleting",rr
                        rr.delete()

                    for cc in cat:
                        print "  - deleting",cc
                        cc.delete()
                else:
                    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--experiment","-e", type=int, nargs='*', default = None)
    parser.add_argument("--category","-c", nargs='*',default = None)
    parser.add_argument("--result","-r",nargs='*', default = None)
    args = parser.parse_args()

    main(args)

