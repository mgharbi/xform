#!/usr/bin/python
# ----------------------------------------------------------------------------
# File:    powerEquation.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2014-10-06
# ----------------------------------------------------------------------------
#
# Estimate energy and time savings provided by a cloud-enabled workflow.
# 
# ----------------------------------------------------------------------------

# Power
P_compute         = 1.5 # Computation power cost on the mobile device in W
P_transfer        = 2.4 # Upload/Download power cost in W
P_idle            = 0.7 # Idling device power cost in W
rho               = P_idle/P_compute
print "--------- Power usage -------------"
print "P_transfer \t= %.1fW" % P_transfer
print "P_compute \t= %.1fW" % P_compute
print "P_idle \t\t= %.1fW" % P_idle
print "rho \t\t= %.2f" % rho
print "-----------------------------------"
print ""

# Data
n_channels        = 3.0
n_pixels          = 8e6
uncompressed_img  = (n_pixels*8*n_channels)     # data to send+receive image as uncompressed bmp
print "--------- Data size  --------------"
print "n_pixels \t= %d MPix" % (n_pixels/1e6)
print "n_channels \t= %d" % n_channels
print "img_size \t= %.2f MB" % (uncompressed_img/(8*1e6))
print "-----------------------------------"
print ""

# Computational efforts
n_instructions    = 10*n_pixels*n_channels     # 32-bit instructions to perform the computation
n_reconstruction  = 6*n_pixels*n_channels       # 32-bit instructions to perform the reconstruction
n_compression     = 10*n_pixels*n_channels      # 32-bit instructions to perform the reconstruction
print "--------- Computational effort ----"
print "n_ins/pix \t= %d" % (n_instructions/n_pixels)
print "n_rec/pix \t= %d" % (n_reconstruction/n_pixels)
print "n_comp/pix \t= %d" % (n_compression/n_pixels)
print "-----------------------------------"
print ""

# Processing power
speed             = 200e6                       # Device's speed in instructions per second
speed_server      = 70e9                        # Server's speed in instructions per second
sigma             = speed_server/speed
# Network
bandwidth         = 2e6                         # Network bandwith in bits/sec
useWifi           = False
if useWifi:
    bandwidth     = 10e6
print "--------- Hardware ----------------"
print "S_client \t= %d MFLOPS" % (speed/1e6)
print "S_server \t= %d MFLOPS" % (speed_server/1e6)
print "sigma \t\t= %.2f" % (speed_server/speed)
print "bandwidth \t= %.2f Mb/sec" % (bandwidth/1e6)
print "-----------------------------------"
print ""

# Upper bound on data
maxDataEnergy     = (n_instructions*(1-rho/sigma) - n_reconstruction - n_compression)
maxDataEnergy    *= (bandwidth/speed)*(P_compute/P_transfer)
maxDataTime       = (n_instructions*(1-1/sigma) - n_reconstruction - n_compression)
maxDataTime      *= (bandwidth/speed)
print "--------- Data constraint ---------"
print "D < %.2f MB (energy constraint)" % (maxDataEnergy/(8*1e6))
print "D < %.2f MB (time constraint)" % (maxDataTime/(8*1e6))
print "-----------------------------------"
print ""

compression_factor_up   = .1
compression_factor_down = .1
data                    = compression_factor_up*uncompressed_img + compression_factor_down*uncompressed_img
uncompressed_data       = 2*uncompressed_img
print "--------- Transfered data ---------"
print "img_size \t= %.2f MB" % (uncompressed_img/(8*1e6))
print "D_uncompressed \t= %.2f MB" % (uncompressed_data/(8*1e6))
print "compress_up \t= %05.2f%%" % (compression_factor_up*100)
print "compress_down \t= %05.2f%%" % (compression_factor_down*100)
print "D_transfered \t= %.2f MB" % (data/(8*1e6))
print "-----------------------------------"
print ""

T_local          = n_instructions/speed
T_transfer       = data/bandwidth
T_server         = n_instructions/speed_server
T_reconstruction = n_reconstruction/speed
T_compression    = n_compression/speed
T_remote         = T_transfer + T_server + T_reconstruction + T_compression
time_saved       = 100*(T_local-T_remote)/T_local
speedup          = T_local/T_remote
print "========= Computation time ========"
print "T_compression \t= %.2fs" % (T_compression)
print "T_transfer \t= %.2fs" % (T_transfer)
print "T_server \t= %.2fs" % (T_server)
print "T_recons \t= %.2fs" % (T_reconstruction)
print ""
print "T_remote \t= %.2fs" % (T_remote)
print "T_local \t= %.2fs" % (T_local)
if speedup > 1:
    print "\t\t  %.2fx speedup" % (speedup)
else:
    print "\t\t  %.2fx slowdown" % (1/speedup)
print "-----------------------------------"
print ""

E_local           = P_compute*T_local
E_transfer        = data*P_transfer/bandwidth
E_reconstruct     = n_reconstruction*P_compute/speed
E_compress        = n_compression*P_compute/speed
E_idle            = n_instructions*P_idle/speed_server
E_remote          = E_transfer + E_reconstruct + E_compress + E_idle

energy_gain      = 100*(E_local - E_remote)/E_local
energy_reduction = (E_local)/E_remote

print "========= Energy cost ============="
print "E_compression \t= %.2fJ" % E_compress
print "E_transfer \t= %.2fJ" % E_transfer
print "E_idle \t\t= %.2fJ" % E_idle
print "E_recons \t= %.2fJ" % E_reconstruct
print ""
print "E_remote \t= %.2fJ" % E_remote
print "E_local \t= %.2fJ" % E_local
if energy_reduction > 1:
    print "\t\t  %.2fx reduction" % (energy_reduction)
else:
    print "\t\t  %.2fx increase" % (1/energy_reduction)
print "-----------------------------------"
print ""
