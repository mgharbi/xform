# -------------------------------------------------------------------
# File:    multiscale_input_transfer.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2014-14-05
# -------------------------------------------------------------------
#
# Generate degraded images for input compression.
#
# ------------------------------------------------------------------#

import os,sys
import re
import shutil
import argparse
import json
import numpy as np

import numpy as np

from PIL import Image

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "transform_compression"))
from settings import *
import utils.filesystem as fs
from mghimproc import imresize, save_and_get_img
from mghimproc import buildLaplacianPyramid, reconstructFromLaplacianPyramid
from mghimproc import color
from scipy.interpolate import interp1d

import input_transfer as xfer


nlevels       = 3
deg_res       = 256*10
ref_res       = 256*10
hist_resample = 1

def dump_json(ff, fsize):
    ff,ext = os.path.splitext(ff)
    ff += ".json"
    f = open(ff,'w')
    f.write(json.dumps({"fsize": fsize}))
    f.close()

def load_json(ff):
    ff,ext = os.path.splitext(ff)
    ff += ".json"
    f = open(ff,'r')
    val = json.loads(f.read())
    f.close()
    return val

def main(category):
    dataDir = DATA_DIR
    ext     = ".png"

    params = [
        {
            "ratio": 2,
            "quality": 60,
        },
        {
            "ratio": 4,
            "quality": 75,
        },
        {
            "ratio": 8,
            "quality": 85,
        },
    ]

    for cat in os.listdir(dataDir):
        if category != "" and cat != category:
            continue
        catDir = os.path.join(dataDir,cat)
        if not os.path.isdir(catDir):
            continue
        for img in os.listdir(catDir):
            imDir = os.path.join(catDir,img)
            if not os.path.isdir(imDir):
                continue

            ref_path = os.path.join(imDir,"unprocessed.png")
            if not os.path.exists(ref_path):
                print "no reference unprocessed, skipping %s" % img
                continue

            print "processing %s %s" % (cat, img)
            Iref = np.squeeze(np.array(Image.open(ref_path))).astype(np.float32)
            Iref = color.RGB_to_YCbCr(Iref)
            Iref = Iref.astype(np.float32)
            hist_ref,rng_ref = xfer.get_histograms(Iref,nlevels, ref_res)
            del Iref

            for p in params:
                ratio   = p["ratio"]
                quality = p["quality"]

                fname    = os.path.join(imDir,"unprocessed_%s_%02d_jpeg_%s%s%s" % ("%s",ratio,"%02d", "%s","%s"))
                fname    = fname % ("us",quality,"_noise",".png")
                fnew,ext = os.path.splitext(fname)
                fnew     = fnew + "_transfer" + ext
                fname = fname.replace("_noise","") # No noise in the input, added during transfer
                print fname
                if not os.path.exists(fname):
                    print "  not found: %s" % os.path.split(fname)[-1]
                    continue
                if os.path.exists(fnew):
                    print "  exists: %s" % os.path.split(fnew)[-1]
                    continue
                print "  %s" % os.path.split(fnew)[-1]

                I = np.squeeze(np.array(Image.open(fname)))
                # I = np.squeeze(np.array(Image.open(fname))).astype(np.float32)

                n_transfer_levels = nlevels

                I = xfer.process(hist_ref,rng_ref, I, transfer_color = True, nlevels = n_transfer_levels, deg_res = deg_res, ref_res = ref_res, hist_resample = hist_resample, output_dir = None)
                save_and_get_img(I, fnew)
                dat = load_json(fname)
                fsize = dat["fsize"]
                dump_json(fnew, fsize)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("category")
    args = parser.parse_args()
    print "Generating degraded inputs in %s" % args.category
    if args.category == 'all':
        allCat = fs.listSubdir(DATA_DIR)
        for c in allCat:
            main(c)
    main(args.category)
