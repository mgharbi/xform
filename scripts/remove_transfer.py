# -------------------------------------------------------------------
# File:    remove_extra_images.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2015-01-14
# -------------------------------------------------------------------
#
#
#
# ------------------------------------------------------------------#

import os,sys
import re
import shutil
import argparse

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "transform_compression"))
from settings import *
import utils.filesystem as fs

def main(category):
    dataDir = DATA_DIR
    ext     = ".png"

    for cat in os.listdir(dataDir):
        if category != "" and cat != category:
            continue
        catDir = os.path.join(dataDir,cat)
        if not os.path.isdir(catDir):
            continue
        for img in os.listdir(catDir):
            imDir = os.path.join(catDir,img)
            if not os.path.isdir(imDir):
                continue
            for f in os.listdir(imDir):
                match = re.match(".*_noise_transfer.*", f)
                if not match:
                    continue
                print "removing %s" % f
                im_path = os.path.join(imDir,f)
                os.remove(im_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("category")
    args = parser.parse_args()
    print "Removing extra files in %s" % args.category
    if args.category == 'all':
        allCat = fs.listSubdir(DATA_DIR)
        for c in allCat:
            main(c)
    else:
        main(args.category)
