import os
import sys
import numpy as np

from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.cm as cmap

from scipy.interpolate import interp1d
import argparse

from settings import DATA_DIR, OUTPUT_DIR
from mghimproc import buildLaplacianPyramid, reconstructFromLaplacianPyramid, buildGaussianPyramid
from mghimproc import color

noisemap = np.random.randn(6000,6000,3)
def add_noise(I, sigma = 1.0, chan = 0):
    sz = I.shape
    I = I.astype(np.float32)
    if len(sz) > 2:
        I = I+sigma*noisemap[:sz[0], :sz[1], :sz[2]]
    else:
        I = I+sigma*noisemap[:sz[0], :sz[1],chan]
    return I


def plot_histograms(I,Id,OO,path):
    # mini = min(np.amin(I[:,:,0]), np.amin(OO[:,:,0]))
    # maxi = max(np.amax(I[:,:,0]), np.amax(OO[:,:,0]))
    mini = np.amin(I)
    maxi = np.amax(I)
    h,bins   = get_histogram(I, mini= mini, maxi = maxi)
    hd,binsd = get_histogram(Id, mini= mini, maxi = maxi)
    ht,binst = get_histogram(OO, mini= mini, maxi = maxi)

    h = np.float32(h)
    hd = np.float32(hd)
    ht = np.float32(ht)

    bins = (bins[:-1]+bins[1:])/2
    binsd = (binsd[:-1]+binsd[1:])/2
    binst = (binst[:-1]+binst[1:])/2
    fig = plt.figure()
    ax =fig.add_subplot(1,1,1)
    ax.plot(bins,h)
    ax.plot(binsd,hd)
    ax.plot(binst,ht)
    ax.legend(["GT","degraded", "hist-transfer"])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    plt.savefig(path)
    plt.clf()

def get_mean_and_variance(h,bins):
    center = .5*(bins[1:]+bins[:-1])
    n = np.sum(h)*1.0
    mu = np.sum(center*h) /n
    var = np.sum(np.square(center)*h) /n - np.square(mu)
    return mu,var


def transfer(Id,hist_ref,rng_ref, deg_res = 256, ref_res = 256*2, hist_resample = 10, transfer_color = True, output_dir = None):

    nlevels = hist_ref[1]

    nit = 1
    if transfer_color:
        nit = 1

    for iteration in range(nit):
        print "iteration %d" % iteration

        LId = buildLaplacianPyramid(Id, nLevels = nlevels + 1)
        LO = []
        # Remap Laplacian
        for il in range(nlevels):
            ld   = LId[il]
            ld = add_noise(ld,sigma = 3.0)

            lo = np.zeros(ld.shape)
            for c in range(ld.shape[2]):
                ldc = ld[:,:,c]
                h,bins    = extract_histogram(hist_ref,rng_ref,il,c)

                f,m,M     = get_transfer_function(ldc,h,bins,deg_res,hist_resample*ref_res)
                lo[:,:,c] = apply_transfer_function(ldc, f,m, M)
                if output_dir is not None:
                    plt.plot(f)
                    plt.savefig(os.path.join(output_dir,"transfer-%02d-%-2d.png" % (il, c)))
                    plt.clf()


            if len(lo.shape) == 2:
                lo.shape += (1,)
            LO.append(lo)
        LO.append(LId[-1])
        Id = reconstructFromLaplacianPyramid(LO)
        Id = np.squeeze(Id)

        # Remap colors
        if transfer_color:
            il = nlevels
            for c in range(Id.shape[2]):
                h,bins    = extract_histogram(hist_ref,rng_ref,il,c)
                f,m,M     = get_transfer_function(Id[:,:,c],h,bins,deg_res,hist_resample*ref_res)
                Id[:,:,c] = apply_transfer_function(Id[:,:,c], f,m, M)
                if output_dir is not None:
                    plt.plot(f)
                    plt.savefig(os.path.join(output_dir,"transfer-color-%-2d.png" % ( c)))
                    plt.clf()


    # Clamp values
    Id[Id<0]   = 0
    Id[Id>255] = 255
    return Id


def get_histograms(I, nlevels = 3, resolution = 256):
    """ Compute rescaling histograms """

    if len(I.shape) > 2:
        nchan = I.shape[2]
    else:
        nchan = 1

    nHistPerChannel = nlevels + 1
    LI            = buildLaplacianPyramid(I,nLevels = nlevels + 1)
    histograms    = np.zeros((nchan*nHistPerChannel*((resolution-1)) + 3,), dtype=np.int32)

    histograms[0] = resolution
    histograms[1] = nlevels
    histograms[2] = nchan
    idx           = 3

    rng = np.zeros(nchan*nHistPerChannel*2, dtype=np.float32)

    for il in range(nlevels):
        l    = LI[il]
        for c in range(nchan):
            h,bins = get_histogram(l[:,:,c], resolution)
            n = len(h)
            rng[2*c+2*nchan*il]   = bins[0]
            rng[2*c+2*nchan*il+1] = bins[-1]
            histograms[idx:idx+n] = h
            idx += n

    # histogram of RGB values
    il = nlevels
    for c in range(nchan):
        h,bins = get_histogram(I[:,:,c], resolution)
        n = len(h)
        rng[2*c+2*nchan*il]   = bins[0]
        rng[2*c+2*nchan*il+1] = bins[-1]
        histograms[idx:idx+n] = h
        idx += n

    return histograms,rng


def extract_histogram(histograms, rng, lvl, chan):
    """ Extract an histogram from the packed data """

    resolution = histograms[0]
    nlevels    = histograms[2]
    nchan      = histograms[2]
    n          = resolution -1

    nHistPerChannel = nlevels + 1
    idx = 3 + lvl*(nchan*((resolution-1))) + chan*((resolution-1))

    mini = rng[2*chan+2*lvl*nchan]
    maxi = rng[2*chan+2*lvl*nchan+1]
    h    = histograms[idx:idx+n]
    bins = np.linspace(mini,maxi,endpoint = True, num = resolution)

    return (h,bins)


def get_histogram(array, resolution = 256, mini = None, maxi = None):
    """ Computes a 1D histogram of the given array """

    array = np.ravel(array)
    if mini is None:
        mini = np.amin(array)
    if maxi is None:
        maxi = np.amax(array)
    bins = np.linspace(mini,maxi,endpoint = True, num = resolution)
    h = np.histogram(array, bins = bins)[0]
    return (h,bins)


def get_transfer_function(source, h2,bins2, res1 = 256, res2 = 256*10):
    """ Computes a 1D histogram remapping function """

    h1,bins1 = get_histogram(source, res1)
    m1 = bins1[0]
    M1 = bins1[-1]
    m2 = bins2[0]
    M2 = bins2[-1]

    h1 = h1.astype(np.float32)
    h2 = h2.astype(np.float32)

    cdf1 = np.cumsum(h1)*1.0/sum(h1)
    cdf2 = np.cumsum(h2)*1.0/sum(h2)


    # Resample in preparation of the inversion
    if res2-1 != len(cdf2):
        old_ticks = np.linspace(0,1,endpoint = True, num = len(cdf2))
        new_ticks = np.linspace(0,1,endpoint = True, num = res2)
        cdf2_i = interp1d(old_ticks, cdf2, kind="linear")
        cdf2_i = cdf2_i(new_ticks)
        # plt.plot(old_ticks,cdf2)
        # plt.plot(new_ticks,cdf2_i)
        # plt.show()
        # cdf2_i[-1] = 1.0
    else:
        cdf2_i = cdf2
    # cdf2_i[cdf2_i<0] = 0.0
    # cdf2_i[cdf2_i>1] = 1.0
    cdf2_i[-1] = 1
    cdf1[-1] = 1

    f = np.zeros((len(cdf1)-1,))
    for i in range(len(f)):
        found = np.where(cdf2_i >= cdf1[i])
        if len(found[0]) == 0:
            print cdf1[i]
            print cdf2_i
        idx = found[0][0]
        f[i] = idx *1.0/ res2
    # f = np.linspace(0,1,endpoint = True, num = len(cdf1)-1)
    # plt.plot(cdf1)
    # plt.plot(f)
    # plt.show()
    return f,m2,M2


def apply_transfer_function(source, f, m2, M2):
    """ Remap values """

    m = np.amin(source)
    M = np.amax(source)
    src = (source-m).astype(np.float64)
    if M != m:
        src /= (M-m)

    n = len(f)-1
    f = np.append(f, f[-1])
    idx  = (np.floor(n*src)).astype(int)
    x      = (n*src-idx);
    O      = ((1-x)*f[idx]+x*f[idx+1]);
    O *= (M2-m2)
    O += m2
    return O


def process(hist_ref,rng_ref, Id, transfer_color = True, nlevels = 3, deg_res = 256, ref_res = 256*2, hist_resample = 10, output_dir = None, I = None):

    Id = color.RGB_to_YCbCr(Id)
    Id = Id.astype(np.float32)

    O = transfer(Id,hist_ref,rng_ref, deg_res, ref_res, hist_resample, transfer_color, output_dir = output_dir)

    O[O<0]   = 0
    O[O>255] = 255
    O = color.YCbCr_to_RGB(O)
    O = O.astype(np.uint8)

    # Debug
    if output_dir is not None and I is not None:
        plot_histograms(I.astype(np.float32),Id.astype(np.float32),O, os.path.join(output_dir,"hist_colors.png"))

        LI  = buildLaplacianPyramid(I.astype(np.float32),nLevels   = nlevels + 1)
        LId = buildLaplacianPyramid(Id.astype(np.float32), nLevels = nlevels + 1)
        LO  = buildLaplacianPyramid(O.astype(np.float32), nLevels = nlevels + 1)

        for c in range(3):
            for il in range(nlevels):
                plot_histograms(LI[il][:,:,c],LId[il][:,:,c],LO[il][:,:,c], os.path.join(output_dir,"hist_lapl-%02d-%02d.png" % (il,c)))

    return O


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filter", default = "style")
    parser.add_argument("-d", type=int, default = 8)
    args = parser.parse_args()
    output_dir = os.path.join(OUTPUT_DIR,"_debug")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if args.filter == "portrait":
        cat     = 'PortraitTransfer'
        example = '0015'
    if args.filter == "style":
        cat     = 'StyleTransfer'
        example = '0002'
    elif args.filter == "dehaze":
        cat     = 'Dehazing'
        example = '0003'
    elif args.filter == "detail":
        cat     = 'DetailManipulation'
        example = '0004'
    in_path = os.path.join(DATA_DIR,cat,example,"unprocessed.png")
    out_path = os.path.join(output_dir,"03-transfered.png" )
    ref_path = os.path.join(output_dir,"01-ref.png" )
    ref_in_path = os.path.join(output_dir,"02-ref_degraded.png" )


    noise = False
    if noise:
        nFlag = "_noise"
    else:
        nFlag = ""

    ds = args.d
    if ds == 2:
        in_deg_path = os.path.join(DATA_DIR,cat,example,"unprocessed_us_02_jpeg_60%s.png" % nFlag)
    elif ds == 4:
        in_deg_path = os.path.join(DATA_DIR,cat,example,"unprocessed_us_04_jpeg_75%s.png" % nFlag)
    else:
        in_deg_path = os.path.join(DATA_DIR,cat,example,"unprocessed_us_08_jpeg_85%s.png" % nFlag)

    # nlevels       = int(np.log2(ds))
    nlevels = 3
    print "%d levels" % nlevels
    ref_res       = 256*10
    deg_res       = 256*10
    hist_resample = 1

    I     = np.array(Image.open(in_path))
    Image.fromarray(I.astype(np.uint8)).save(ref_path)
    I = color.RGB_to_YCbCr(I)
    I = I.astype(np.float32)
    hist_ref,rng_ref = get_histograms(I,nlevels,ref_res)

    Id    = np.array(Image.open(in_deg_path))
    Image.fromarray(Id.astype(np.uint8)).save(ref_in_path)
    Id = color.RGB_to_YCbCr(Id)
    Id = Id.astype(np.float32)

    O = process(hist_ref,rng_ref, Id, transfer_color = True, nlevels = nlevels, deg_res = deg_res, ref_res = ref_res, hist_resample = hist_resample, output_dir = output_dir, I = I)

    O[O<0]   = 0
    O[O>255] = 255
    O = color.YCbCr_to_RGB(O)
    O = O.astype(np.uint8)
    Image.fromarray(O).save(out_path)
