# ----------------------------------------------------------------------------
# File:    data_io.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2015-02-11
# ----------------------------------------------------------------------------
#
# Load input images, and export reconstructed image
#
# ---------------------------------------------------------------------------#

import sys
# sys.path.append("/Users/mgharbi/Documents/projects/xform_mobile/python")
# import xform

import os, re, sys
from settings import *
from scipy.misc import imread, imsave
import numpy as np
from pipeline_node import PipelineNode
from mghimproc import imresize, save_and_get_img
from PIL import Image
from io import BytesIO


from transform_model import get_transform_model
from PIL import Image

class ImageInitializer(PipelineNode):
    def process(self,r):
        xform_mdl = get_transform_model(self.p)
        I,isize   = self.load(r.unprocessedPath())
        O,osize   = self.load(r.processedPath())
        self.validate(I,O)

        model               = xform_mdl(I,O,self.p)
        model.r             = r
        model._nbytes_input = isize
        return (model,)


    def validate(self,I,O):
        if I.shape[0] != O.shape[0] or I.shape[1] != O.shape[1]:
            raise IOError(0, "I and O shape dont match")


    def load(self,path):
        """Load image, trying other extensions if not found"""

        path, name = os.path.split(path)
        name, ext  = os.path.splitext(name)
        # Try other extensions if file not found
        if not os.path.exists(os.path.join(path,name+ext)):
            # Extension search
            for f in os.listdir(path):
                match = re.match(name+"\.(png|jpg|jpeg)", f)
                if match:
                    name = f
                    break
        else:
            name = name+ext

        path    = os.path.join(path, name)
        print path
        im_file = Image.open(path)
        I       = np.array(im_file)
        fsize   = os.stat(path).st_size

        # Make sure grey images have 3 dimensions
        if len(I.shape) < 3:
            I.shape += (1,)

        return I,fsize

# class LocalLaplacian(ImageInitializer):
#     def process(self,r):
#         xform_mdl = get_transform_model(self.p)
#         I,isize   = self.load(r.unprocessedPath())
#         I = I.astype(np.float32)/255.0
#
#         # Compute reference
#         print "      - Laplacian filtering"
#         O = xform.local_laplacian_filter(I,8,5)
#         I = (I*255).astype(np.uint8)
#         O = (O*255).astype(np.uint8)
#         model = xform_mdl(I,O,self.p)
#         model.r             = r
#         model._nbytes_input = isize
#
#
#         # Compute degraded
#         print "      - Laplacian degraded filtering"
#         Id = np.copy(I)
#         Id = imresize(Id,1.0/self.p.in_downsampling, method = "nearest")
#         bio = BytesIO()
#         Image.fromarray(Id).save(bio, format = 'JPEG',quality = self.p.in_quality)
#         nbytes = len(bio.getvalue())
#         bio.seek(0)
#         Id = np.array(Image.open(bio))
#         if self.p.upsample:
#             Id = imresize(Id,I.shape[0:2], method = "nearest")
#         Id = Id.astype(np.float32)/255.0
#         if self.p.upsample and self.p.add_noise:
#             sigma = 5e-3
#             Id += sigma*np.random.randn(Id.shape[0],Id.shape[1],Id.shape[2]).astype(np.float32)
#         Od = xform.local_laplacian_filter(Id,8,5)
#         Id = (Id*255).astype(np.uint8)
#         Od = (Od*255).astype(np.uint8)
#
#         model.I             = Id
#         model.O             = Od
#         model._nbytes_input = nbytes
#
#         return (model,)


class ImageExporter(PipelineNode):
    def process(self,model):
        if not os.path.exists(model.r.outputPath):
            os.makedirs(model.r.outputPath)
        save_and_get_img(np.squeeze(model.R),model.r.reconstructedPath(),quality = 90)
        return (model,)

    def save(self,R, path):
        d = os.path.dirname(path)
        if not os.path.exists(d):
           os.makedirs(d)

        sz = R.shape

        # Collapse singleton dimension (for grey images)
        R = np.squeeze(R)

        imsave(path,R)
