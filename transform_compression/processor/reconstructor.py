# ----------------------------------------------------------------------------
# File:    reconstructor.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2015-02-12
# ----------------------------------------------------------------------------
#
#
#
# ---------------------------------------------------------------------------#


from pipeline_node import PipelineNode
from mghimproc import DifferenceImage
from mghimproc import float2uint8
from mghimproc import image_metrics as metrics
import numpy as np

class Reconstructor(PipelineNode):
    def process(self,model):
        model.reconstruct()
        return (model,)

class JPEGresidual(PipelineNode):
    def process(self,model):
        dd = DifferenceImage(model.O,model.R)
        dd.compress(quality = self.p.jpeg_quality, separate_channels = False,ext=".jpg")
        fsize = dd.file_size
        dd = dd.getDifference()
        if len(dd.shape) < 3:
            dd.shape += (1,)
        model.R = np.float32(model.R)+dd
        model.R = float2uint8(model.R)
        model._nbytes += fsize

        return (model,)
