# ----------------------------------------------------------------------------
# File:    run.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2015-02-11
# ----------------------------------------------------------------------------
#
# Main entry point to the recipe algorithm.
#
# ---------------------------------------------------------------------------#


import sys

#!/usr/bin/python
import sys, os
import argparse

# Virtual env
# if os.getenv("DJANGO_ENV") == "PROD":
#     venv = '/afs/csail.mit.edu/u/g/gharbi/.virtualenvs/default/bin/activate_this.py'
#     execfile(venv, dict(__file__=venv))
#     sys.path.insert(0, "/afs/csail.mit.edu/u/g/gharbi/.virtualenvs/default")
os.chdir(os.path.dirname(os.path.abspath(__file__)))

from settings import *
import utils.filesystem as fs
from processor.processor import Processor

def main(args):
    """Runs the pipeline on given input/output pair, category and experiment.

    :arg1: @todo
    :returns: @todo

    """

    # Django database connection
    sys.path = sys.path + [os.path.join(BASE_DIR,"analysis")]
    if __name__ == "__main__":
        os.environ.setdefault("DJANGO_SETTINGS_MODULE","analysis.settings")
    from summary.models import Result, Experiment, Category
    import django
    django.setup()

    if Experiment.objects.filter(id=args.experiment_id).exists():
        e = Experiment.objects.get(id=args.experiment_id)
    else:
        raise NameError("No such experiment exists: %d" % args.experiment_id)

    # Setup params
    params    = e.parameters
    processor = Processor(params)

    outputDir = os.path.join(OUTPUT_DIR,"%d" % args.experiment_id,args.category)
    inputDir  = os.path.join(DATA_DIR, args.category)

    if Category.objects.filter(name=args.category).exists():
        c = Category.objects.get(name=args.category)
    else:
        raise NameError("No such category found: %s" % args.category)

    if Result.objects.filter(category=c, experiment=e, name=args.name).exists():
        r = Result.objects.get(category=c, experiment=e, name=args.name)
    else:
        r = Result()

    r.category             = c
    r.experiment           = e
    r.name                 = args.name
    r.dataPath             = os.path.join(inputDir,args.name)
    r.outputPath           = os.path.join(outputDir,args.name)
    r.error                = ""

    # if os.getenv("DJANGO_ENV") == "PROD":
    #     try:
    #         r = processor.process(r)
    #     except Exception as e:
    #         print "      error: %s" % e
    #         r.error = e
    # else:
    r = processor.process(r)
    r.save()

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Run the algorithm on the selected image")
    parser.add_argument('experiment_id', type=int,  help='experiment id')
    parser.add_argument('category', help='category of the image to process')
    parser.add_argument('name', help='name of the image to process')
    args = parser.parse_args()
    main(args)
