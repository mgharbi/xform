# ----------------------------------------------------------------------------
# File:    filesystem.py
# Author:  Michael Gharbi <gharbi@mit.edu>
# Created: 2015-02-11
# ----------------------------------------------------------------------------
#
# Helper functions for filesystem interaction.
#
# ---------------------------------------------------------------------------#

import os

def listSubdir(f):
    dirs = os.listdir(f)
    dirs = [ c for c in dirs if os.path.isdir(os.path.join(f,c)) ]
    return dirs
