from settings import *
from processor.processor import Processor

import sys

#!/usr/bin/python
import sys, os
import argparse
from PIL import Image
import numpy as np

from processor import *
from settings import *

# Go to root
os.chdir(os.path.dirname(os.path.abspath(__file__)))

def load(path):
    I = Image.open(path)
    I = np.array(I)
    return I

def save(I, path):
    Image.fromarray(I).save(path, quality = 100)

def main(args):
    sys.path = sys.path + [os.path.join(BASE_DIR,"analysis")]
    os.environ.setdefault("DJANGO_SETTINGS_MODULE","analysis.settings")
    from summary.models import Result, Experiment, Category, Parameters
    import django
    django.setup()

    outputDir = os.path.join(OUTPUT_DIR,"_transfer")
    inputDir  = os.path.join(DATA_DIR, "test_transfer")
    I1_path   = os.path.join(inputDir, "unprocessed_1.jpg")
    I2_path   = os.path.join(inputDir, "unprocessed_2.jpg")
    O1_path   = os.path.join(inputDir, "processed_1.jpg")
    O2_path   = os.path.join(inputDir, "processed_2.jpg")
    if not os.path.exists(outputDir):
        os.makedirs(outputDir)

    I_1 = load(I1_path)
    O_1 = load(O1_path)
    I_2 = load(I2_path)
    O_2 = load(O2_path)

    # Params
    p                        = Parameters()
    p.in_downsampling        = 1
    p.upsample               = True
    p.wSize                  = 1000
    p.use_patch_overlap      = True
    p.epsilon                = 1e-2
    p.use_ratio_of_residuals = True
    p.use_multiscale_feat    = True
    p.use_tonecurve_feat     = True
    p.use_stack              = True
    p.luma_bands             = 6
    p.ms_levels              = -1
    p.transfer_multiscale    = False

    # Fit
    m = RecipeModel(I_1,O_1,p)
    m = RGB2YCbCr().process(m)[0]
    m.build()


    # Swap inputs
    m.Iref = I_2
    # m.I    = I_2
    m.Oref = O_2
    m.O    = O_2
    m = RGB2YCbCr().process(m)[0]

    # Reconstruct
    m.reconstruct()
    m = YCbCr2RGB().process(m)[0]
    m = ClampToUint8().process(m)[0]

    save(m.R, os.path.join(outputDir,"transfer.jpg"))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("mode")
    args = parser.parse_args()
    main(args)
